ASITUS_THIRDPARTY_PRODUCT("occ_prod" "" "ShapeConvert.hxx" "TKCR")

message (STATUS "... OCCT products Include dirs: ${3RDPARTY_occ_prod_INCLUDE_DIR}")
message (STATUS "... OCCT products Library dirs: ${3RDPARTY_occ_prod_LIBRARY_DIR}")
message (STATUS "... OCCT products Binary  dirs: ${3RDPARTY_occ_prod_DLL_DIR}")

string (REPLACE lib libd 3RDPARTY_occ_prod_LIBRARY_DIR_DEBUG ${3RDPARTY_occ_prod_LIBRARY_DIR})
if (3RDPARTY_occ_prod_LIBRARY_DIR_DEBUG AND EXISTS "${3RDPARTY_occ_prod_LIBRARY_DIR_DEBUG}")
  if (WIN32)
    if (NOT EXISTS "${3RDPARTY_occ_prod_LIBRARY_DIR_DEBUG}/TKCR.lib")
      set (3RDPARTY_occ_prod_LIBRARY_DIR_DEBUG "" CACHE INTERNAL FORCE)
    endif()
  else()
    if (NOT EXISTS "${3RDPARTY_occ_prod_LIBRARY_DIR_DEBUG}/libTKCR.so")
      set (3RDPARTY_occ_prod_LIBRARY_DIR_DEBUG "" CACHE INTERNAL FORCE)
    endif()
  endif()
endif()

if (WIN32)
  string (REPLACE bin bind 3RDPARTY_occ_prod_DLL_DIR_DEBUG ${3RDPARTY_occ_prod_DLL_DIR})
  if (3RDPARTY_occ_prod_DLL_DIR_DEBUG AND EXISTS "${3RDPARTY_occ_prod_DLL_DIR_DEBUG}")
    if (NOT EXISTS "${3RDPARTY_occ_prod_DLL_DIR_DEBUG}/TKCR.dll")
      set (3RDPARTY_occ_prod_DLL_DIR_DEBUG "" CACHE INTERNAL FORCE)
    endif()
  endif()
endif()

message (STATUS "... Canonical Recognition Debug Library dirs: ${3RDPARTY_occ_prod_LIBRARY_DIR_DEBUG}")
message (STATUS "... Canonical Recognition Debug Binary  dirs: ${3RDPARTY_occ_prod_DLL_DIR_DEBUG}")

#--------------------------------------------------------------------------
# Installation
if (WIN32)
  install (FILES ${3RDPARTY_occ_prod_DLL_DIR}/TKCR.dll DESTINATION bin)
  install (FILES ${3RDPARTY_occ_prod_DLL_DIR}/TKEMesh.dll DESTINATION bin)
  install (FILES ${3RDPARTY_occ_prod_DLL_DIR}/TKOCCLicense.dll DESTINATION bin)
endif()
