ASITUS_THIRDPARTY_PRODUCT("asitus" "" "asiAlgo.h" "asiAlgo")

message (STATUS "... A-Situs Include dirs: ${3RDPARTY_asitus_INCLUDE_DIR}")
message (STATUS "... A-Situs Library dirs: ${3RDPARTY_asitus_LIBRARY_DIR}")
message (STATUS "... A-Situs Binary  dirs: ${3RDPARTY_asitus_DLL_DIR}")

string (REPLACE lib libd 3RDPARTY_asitus_LIBRARY_DIR_DEBUG ${3RDPARTY_asitus_LIBRARY_DIR})
if (3RDPARTY_asitus_LIBRARY_DIR_DEBUG AND EXISTS "${3RDPARTY_asitus_LIBRARY_DIR_DEBUG}")
  if (WIN32)
    if (NOT EXISTS "${3RDPARTY_asitus_LIBRARY_DIR_DEBUG}/asiAlgo.lib")
      set (3RDPARTY_asitus_LIBRARY_DIR_DEBUG "" CACHE INTERNAL FORCE)
    endif()
  else()
    if (NOT EXISTS "${3RDPARTY_asitus_LIBRARY_DIR_DEBUG}/libasiAlgo.so")
      set (3RDPARTY_asitus_LIBRARY_DIR_DEBUG "" CACHE INTERNAL FORCE)
    endif()
  endif()
endif()

if (WIN32)
  string (REPLACE bin bind 3RDPARTY_asitus_DLL_DIR_DEBUG ${3RDPARTY_asitus_DLL_DIR})
  if (3RDPARTY_asitus_DLL_DIR_DEBUG AND EXISTS "${3RDPARTY_asitus_DLL_DIR_DEBUG}")
    if (NOT EXISTS "${3RDPARTY_asitus_DLL_DIR_DEBUG}/asiAlgo.dll")
      set (3RDPARTY_asitus_DLL_DIR_DEBUG "" CACHE INTERNAL FORCE)
    endif()
  endif()
endif()

message (STATUS "... Mobius Debug Library dirs: ${3RDPARTY_asitus_LIBRARY_DIR_DEBUG}")
message (STATUS "... Mobius Debug Binary  dirs: ${3RDPARTY_asitus_DLL_DIR_DEBUG}")

#--------------------------------------------------------------------------
# Installation
if (WIN32)
  install (FILES ${3RDPARTY_asitus_DLL_DIR}/asiAlgo.dll DESTINATION bin)
  install (FILES ${3RDPARTY_asitus_DLL_DIR}/asiData.dll DESTINATION bin)
  install (FILES ${3RDPARTY_asitus_DLL_DIR}/asiEngine.dll DESTINATION bin)
  install (FILES ${3RDPARTY_asitus_DLL_DIR}/asiUI.dll DESTINATION bin)
  install (FILES ${3RDPARTY_asitus_DLL_DIR}/asiVisu.dll DESTINATION bin)
endif()
