//-----------------------------------------------------------------------------
// Created on: 23 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_StockFunc.h>

// Active Data includes
#include <ActData_ParameterFactory.h>

// OCCT includes
#include <BRepPrimAPI_MakeBox.hxx>

//-----------------------------------------------------------------------------

//! \return Tree Function instance.
Handle(exeCSG_StockFunc) exeCSG_StockFunc::Instance()
{
  return new exeCSG_StockFunc();
}

//-----------------------------------------------------------------------------

//! \return GUID.
const char* exeCSG_StockFunc::GUID()
{
  return "18B61126-D558-407E-A194-DAE5E47B2974";
}

//-----------------------------------------------------------------------------

//! \return GUID.
const char* exeCSG_StockFunc::GetGUID() const
{
  return GUID();
}

//-----------------------------------------------------------------------------

//! \return human-readable name of Tree Function.
const char* exeCSG_StockFunc::GetName() const
{
  return "Stock self-evaluation";
}

//-----------------------------------------------------------------------------

//! Executes Tree Function.
//! \param argsIn   [in] INPUT Parameters.
//! \param argsOut  [in] OUTPUT Parameters
//! \param userData [in] user data.
//! \param PEntry   [in] Progress Entry.
//! \return execution status.
int exeCSG_StockFunc::execute(const Handle(ActAPI_HParameterList)& argsIn,
                              const Handle(ActAPI_HParameterList)& argsOut,
                              const Handle(Standard_Transient)&    ActData_NotUsed(userData),
                              ActAPI_ProgressEntry                 PEntry) const
{
  /* ============================
   *  Interpret INPUT Parameters
   * ============================ */

  const double left_X  = ActParamTool::AsReal( argsIn->Value(1) )->GetValue();
  const double left_Y  = ActParamTool::AsReal( argsIn->Value(2) )->GetValue();
  const double left_Z  = ActParamTool::AsReal( argsIn->Value(3) )->GetValue();
  const double right_X = ActParamTool::AsReal( argsIn->Value(4) )->GetValue();
  const double right_Y = ActParamTool::AsReal( argsIn->Value(5) )->GetValue();
  const double right_Z = ActParamTool::AsReal( argsIn->Value(6) )->GetValue();

  /* =============================
   *  Interpret OUTPUT Parameters
   * ============================= */

  Handle(ActData_ShapeParameter)
    shapeOutParam = ActParamTool::AsShape( argsOut->Value(1) );

  /* =======================
   *  Construct stock shape
   * ======================= */

  TopoDS_Shape box = BRepPrimAPI_MakeBox( gp_Pnt(left_X, left_Y, left_Z),
                                          gp_Pnt(right_X, right_Y, right_Z) );

  /* ==================================
   *  Set results to OUTPUT Parameters
   * ================================== */

  shapeOutParam->SetShape(box, MT_Impacted);

  // Put logging message
  PEntry.SendLogMessage(LogInfo(Normal) << "Stock was created successfully.");

  return 0; // OK
}

//-----------------------------------------------------------------------------

//! Returns accepted INPUT signature for VALIDATION.
//! \return expected INPUT signature.
ActAPI_ParameterTypeStream exeCSG_StockFunc::inputSignature() const
{
  return ActAPI_ParameterTypeStream() << Parameter_Real
                                      << Parameter_Real
                                      << Parameter_Real
                                      << Parameter_Real
                                      << Parameter_Real
                                      << Parameter_Real;
}

//-----------------------------------------------------------------------------

//! Returns accepted OUTPUT signature for VALIDATION.
//! \return expected OUTPUT signature.
ActAPI_ParameterTypeStream exeCSG_StockFunc::outputSignature() const
{
  return ActAPI_ParameterTypeStream() << Parameter_Shape;
}
