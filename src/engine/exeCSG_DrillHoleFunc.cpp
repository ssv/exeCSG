//-----------------------------------------------------------------------------
// Created on: 26 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_DrillHoleFunc.h>

// Active Data includes
#include <ActData_ParameterFactory.h>

// OCCT includes
#include <BRepPrimAPI_MakeCylinder.hxx>
#include <gp_Ax3.hxx>

//-----------------------------------------------------------------------------

//! \return Tree Function instance.
Handle(exeCSG_DrillHoleFunc) exeCSG_DrillHoleFunc::Instance()
{
  return new exeCSG_DrillHoleFunc();
}

//-----------------------------------------------------------------------------

//! \return GUID.
const char* exeCSG_DrillHoleFunc::GUID()
{
  return "A14A119B-5817-4158-A020-5B966CB51A19";
}

//-----------------------------------------------------------------------------

//! \return GUID.
const char* exeCSG_DrillHoleFunc::GetGUID() const
{
  return GUID();
}

//-----------------------------------------------------------------------------

//! \return human-readable name of Tree Function.
const char* exeCSG_DrillHoleFunc::GetName() const
{
  return "Drill hole self-evaluation";
}

//-----------------------------------------------------------------------------

//! Executes Tree Function.
//! \param argsIn   [in] INPUT Parameters.
//! \param argsOut  [in] OUTPUT Parameters
//! \param userData [in] user data.
//! \param PEntry   [in] Progress Entry.
//! \return execution status.
int exeCSG_DrillHoleFunc::execute(const Handle(ActAPI_HParameterList)& argsIn,
                                  const Handle(ActAPI_HParameterList)& argsOut,
                                  const Handle(Standard_Transient)&    ActData_NotUsed(userData),
                                  ActAPI_ProgressEntry                 PEntry) const
{
  /* ============================
   *  Interpret INPUT Parameters
   * ============================ */

  const double O_x    = ActParamTool::AsReal( argsIn->Value(1) )->GetValue();
  const double O_y    = ActParamTool::AsReal( argsIn->Value(2) )->GetValue();
  const double O_z    = ActParamTool::AsReal( argsIn->Value(3) )->GetValue();
  const double n_x    = ActParamTool::AsReal( argsIn->Value(4) )->GetValue();
  const double n_y    = ActParamTool::AsReal( argsIn->Value(5) )->GetValue();
  const double n_z    = ActParamTool::AsReal( argsIn->Value(6) )->GetValue();
  const double ksi_x  = ActParamTool::AsReal( argsIn->Value(7) )->GetValue();
  const double ksi_y  = ActParamTool::AsReal( argsIn->Value(8) )->GetValue();
  const double ksi_z  = ActParamTool::AsReal( argsIn->Value(9) )->GetValue();
  const double radius = ActParamTool::AsReal( argsIn->Value(10) )->GetValue();
  const double depth  = ActParamTool::AsReal( argsIn->Value(11) )->GetValue();

  /* =============================
   *  Interpret OUTPUT Parameters
   * ============================= */

  Handle(ActData_ShapeParameter) outParam = ActParamTool::AsShape( argsOut->Value(1) );

  /* =================
   *  Construct shape
   * ================= */

  gp_Ax3 placement( gp_Pnt(O_x, O_y, O_z),
                    gp_Dir(n_x, n_y, n_z),
                    gp_Dir(ksi_x, ksi_y, ksi_z) );

  // Construct feature shape
  TopoDS_Shape feature = BRepPrimAPI_MakeCylinder( placement.Ax2(), radius, depth );

  /* ==================================
   *  Set results to OUTPUT Parameters
   * ================================== */

  // Set output
  outParam->SetShape(feature, MT_Impacted);

  // Put logging message
  PEntry.SendLogMessage(LogInfo(Normal) << "Drill hole was self-evaluated.");

  return 0; // OK
}

//-----------------------------------------------------------------------------

//! Returns accepted INPUT signature for VALIDATION.
//! \return expected INPUT signature.
ActAPI_ParameterTypeStream exeCSG_DrillHoleFunc::inputSignature() const
{
  return ActAPI_ParameterTypeStream() << Parameter_Real  // O_x
                                      << Parameter_Real  // O_y
                                      << Parameter_Real  // O_z
                                      << Parameter_Real  // n_x
                                      << Parameter_Real  // n_y
                                      << Parameter_Real  // n_z
                                      << Parameter_Real  // Ksi_x
                                      << Parameter_Real  // Ksi_y
                                      << Parameter_Real  // Ksi_z
                                      << Parameter_Real  // Radius
                                      << Parameter_Real; // Depth
}

//-----------------------------------------------------------------------------

//! Returns accepted OUTPUT signature for VALIDATION.
//! \return expected OUTPUT signature.
ActAPI_ParameterTypeStream exeCSG_DrillHoleFunc::outputSignature() const
{
  return ActAPI_ParameterTypeStream() << Parameter_Shape;
}
