//-----------------------------------------------------------------------------
// Created on: 22 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_Commands_h
#define exeCSG_Commands_h

// exeCSG includes
#include <exeCSG_Model.h>

//! Business logic bound to CSG parametric modeling data.
class exeCSG_Commands
{
public:

  //! ctor accepting Data Model instance to operate on.
  //! \param[in] model Data Model instance.
  exeCSG_Commands(const Handle(exeCSG_Model)& model) : m_model(model) {}

public:

  //! Creates Stock Node.
  //! \return newly created Stock Node.
  Handle(exeCSG_StockNode)
    CreateStockNode();

  //! Creates Workpiece Node.
  //! \return newly created Workpiece Node.
  Handle(exeCSG_WorkpieceNode)
    CreateWorkpieceNode();

  //! Creates Drill Hole Node.
  //! \return newly created Drill Hole Node.
  Handle(exeCSG_DrillHoleNode)
    CreateDrillHoleNode(const gp_Ax3& placement,
                        const double  radius,
                        const double  depth);

  //! Creates Rectangular Hole Node.
  //! \return newly created Rectangular Hole Node.
  Handle(exeCSG_RectHoleNode)
    CreateRectHoleNode(const gp_Ax3& placement,
                       const double  width,
                       const double  height,
                       const double  depth);

  //! Reconnects Tree Functions used for workpiece evaluation.
  void
    ReconnectWorkpieceFunctions();

private:

  Handle(exeCSG_Model) m_model; //!< Data Model instance.

};

#endif
