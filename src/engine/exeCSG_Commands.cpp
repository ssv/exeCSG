//-----------------------------------------------------------------------------
// Created on: 22 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_Commands.h>

// exeCSG includes
#include <exeCSG_DrillHoleFunc.h>
#include <exeCSG_MillingFunc.h>
#include <exeCSG_RectHoleFunc.h>
#include <exeCSG_StockFunc.h>

//-----------------------------------------------------------------------------

Handle(exeCSG_StockNode) exeCSG_Commands::CreateStockNode()
{
  // Settle down the Node to OCAF
  Handle(exeCSG_StockNode)
    stockNode = Handle(exeCSG_StockNode)::DownCast( exeCSG_StockNode::Instance() );
  //
  m_model->GetStockPartition()->AddNode(stockNode);

  // Initialize the Node with default values
  stockNode->Init();
  stockNode->SetName("Stock");

  // Connect Tree Function for self-evaluation of stock
  stockNode->ConnectTreeFunction( exeCSG_StockNode::PID_FuncSelfEval,
                                  exeCSG_StockFunc::GUID(),
                                  ActAPI_ParameterStream() << stockNode->Parameter(exeCSG_StockNode::PID_LeftX)
                                                           << stockNode->Parameter(exeCSG_StockNode::PID_LeftY)
                                                           << stockNode->Parameter(exeCSG_StockNode::PID_LeftZ)
                                                           << stockNode->Parameter(exeCSG_StockNode::PID_RightX)
                                                           << stockNode->Parameter(exeCSG_StockNode::PID_RightY)
                                                           << stockNode->Parameter(exeCSG_StockNode::PID_RightZ),
                                  ActAPI_ParameterStream() << stockNode->Parameter(exeCSG_StockNode::PID_Shape) );

  // Add Node as a child to the root Node
  m_model->GetRootNode()->AddChildNode(stockNode);

  return stockNode;
}

//-----------------------------------------------------------------------------

Handle(exeCSG_WorkpieceNode) exeCSG_Commands::CreateWorkpieceNode()
{
  // Settle down the Node to OCAF
  Handle(exeCSG_WorkpieceNode)
    workpieceNode = Handle(exeCSG_WorkpieceNode)::DownCast( exeCSG_WorkpieceNode::Instance() );
  //
  m_model->GetWorkpiecePartition()->AddNode(workpieceNode);

  // Initialize the Node with default values
  workpieceNode->Init();
  workpieceNode->SetName("Workpiece");

  // Add Node as a child to the root Node
  m_model->GetRootNode()->AddChildNode(workpieceNode);

  return workpieceNode;
}

//-----------------------------------------------------------------------------

Handle(exeCSG_DrillHoleNode)
  exeCSG_Commands::CreateDrillHoleNode(const gp_Ax3& placement,
                                       const double  radius,
                                       const double  depth)
{
  // Settle down the Node to OCAF
  Handle(exeCSG_DrillHoleNode)
    featureNode = Handle(exeCSG_DrillHoleNode)::DownCast( exeCSG_DrillHoleNode::Instance() );
  //
  m_model->GetDrillHolePartition()->AddNode(featureNode);

  // Initialize the Node with the passed parameter values
  featureNode->Init(placement, radius, depth);
  featureNode->SetName("Drill hole");

  // Add Node as a child to the Stock Node
  m_model->GetStockNode()->AddChildNode(featureNode);

  // Connect Tree Function for self-evaluation of a feature
  featureNode->ConnectTreeFunction( exeCSG_DrillHoleNode::PID_FuncSelfEval,
                                    exeCSG_DrillHoleFunc::GUID(),
                                    ActAPI_ParameterStream() << featureNode->Parameter(exeCSG_FeatureNode::PID_OX)
                                                             << featureNode->Parameter(exeCSG_FeatureNode::PID_OY)
                                                             << featureNode->Parameter(exeCSG_FeatureNode::PID_OZ)
                                                             << featureNode->Parameter(exeCSG_FeatureNode::PID_ZetaX)
                                                             << featureNode->Parameter(exeCSG_FeatureNode::PID_ZetaY)
                                                             << featureNode->Parameter(exeCSG_FeatureNode::PID_ZetaZ)
                                                             << featureNode->Parameter(exeCSG_FeatureNode::PID_KsiX)
                                                             << featureNode->Parameter(exeCSG_FeatureNode::PID_KsiY)
                                                             << featureNode->Parameter(exeCSG_FeatureNode::PID_KsiZ)
                                                             << featureNode->Parameter(exeCSG_DrillHoleNode::PID_Radius)
                                                             << featureNode->Parameter(exeCSG_DrillHoleNode::PID_Depth),
                                  ActAPI_ParameterStream() << featureNode->Parameter(exeCSG_DrillHoleNode::PID_Shape) );

  // Reconnect Tree Functions for Workpiece Node
  this->ReconnectWorkpieceFunctions();

  return featureNode;
}

//-----------------------------------------------------------------------------

Handle(exeCSG_RectHoleNode)
  exeCSG_Commands::CreateRectHoleNode(const gp_Ax3& placement,
                                      const double  width,
                                      const double  height,
                                      const double  depth)
{
  // Settle down the Node to OCAF
  Handle(exeCSG_RectHoleNode)
    featureNode = Handle(exeCSG_RectHoleNode)::DownCast( exeCSG_RectHoleNode::Instance() );
  //
  m_model->GetRectHolePartition()->AddNode(featureNode);

  // Initialize the Node with the passed parameter values
  featureNode->Init(placement, width, height, depth);
  featureNode->SetName("Rectangular hole");

  // Add Node as a child to the Stock Node
  m_model->GetStockNode()->AddChildNode(featureNode);

  // Connect Tree Function for self-evaluation of a feature
  featureNode->ConnectTreeFunction( exeCSG_RectHoleNode::PID_FuncSelfEval,
                                    exeCSG_RectHoleFunc::GUID(),
                                    ActAPI_ParameterStream() << featureNode->Parameter(exeCSG_FeatureNode::PID_OX)
                                                             << featureNode->Parameter(exeCSG_FeatureNode::PID_OY)
                                                             << featureNode->Parameter(exeCSG_FeatureNode::PID_OZ)
                                                             << featureNode->Parameter(exeCSG_FeatureNode::PID_ZetaX)
                                                             << featureNode->Parameter(exeCSG_FeatureNode::PID_ZetaY)
                                                             << featureNode->Parameter(exeCSG_FeatureNode::PID_ZetaZ)
                                                             << featureNode->Parameter(exeCSG_FeatureNode::PID_KsiX)
                                                             << featureNode->Parameter(exeCSG_FeatureNode::PID_KsiY)
                                                             << featureNode->Parameter(exeCSG_FeatureNode::PID_KsiZ)
                                                             << featureNode->Parameter(exeCSG_RectHoleNode::PID_Width)
                                                             << featureNode->Parameter(exeCSG_RectHoleNode::PID_Height)
                                                             << featureNode->Parameter(exeCSG_RectHoleNode::PID_Depth),
                                  ActAPI_ParameterStream() << featureNode->Parameter(exeCSG_RectHoleNode::PID_Shape) );

  // Reconnect Tree Functions for Workpiece Node
  this->ReconnectWorkpieceFunctions();

  return featureNode;
}

//-----------------------------------------------------------------------------

void exeCSG_Commands::ReconnectWorkpieceFunctions()
{
  ActAPI_ParameterStream inputs;

  // Get Stock Node
  Handle(exeCSG_StockNode) stock = m_model->GetStockNode();

  // Add stock shape as a first input
  inputs << stock->Parameter(exeCSG_StockNode::PID_Shape);

  // Loop over the feature Nodes and set their self outputs as inputs
  for ( Handle(ActAPI_IChildIterator) cit = stock->GetChildIterator(); cit->More(); cit->Next() )
  {
    Handle(ActAPI_INode) featNode = cit->Value();

    int PID = -1;
    //
    if ( featNode->IsInstance( STANDARD_TYPE(exeCSG_DrillHoleNode) ) )
      PID = exeCSG_DrillHoleNode::PID_Shape;
    //
    else if ( featNode->IsInstance( STANDARD_TYPE(exeCSG_RectHoleNode) ) )
      PID = exeCSG_RectHoleNode::PID_Shape;

    if ( PID != -1 )
      inputs << featNode->Parameter(PID);
  }

  // Get Workpiece Node
  Handle(exeCSG_WorkpieceNode) workpiece = m_model->GetWorkpieceNode();

  // Connect Tree Function for evaluation of workpiece
  workpiece->ConnectTreeFunction( exeCSG_WorkpieceNode::PID_FuncMillingEval,
                                  exeCSG_MillingFunc::GUID(),
                                  inputs,
                                  ActAPI_ParameterStream() << workpiece->Parameter(exeCSG_WorkpieceNode::PID_Shape) );
}
