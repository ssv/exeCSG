//-----------------------------------------------------------------------------
// Created on: 26 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_RectHoleFunc.h>

// Active Data includes
#include <ActData_ParameterFactory.h>

//-----------------------------------------------------------------------------

//! \return Tree Function instance.
Handle(exeCSG_RectHoleFunc) exeCSG_RectHoleFunc::Instance()
{
  return new exeCSG_RectHoleFunc();
}

//-----------------------------------------------------------------------------

//! \return GUID.
const char* exeCSG_RectHoleFunc::GUID()
{
  return "2CECC6DA-654A-4FCC-BE6B-F7E98995EEAE";
}

//-----------------------------------------------------------------------------

//! \return GUID.
const char* exeCSG_RectHoleFunc::GetGUID() const
{
  return GUID();
}

//-----------------------------------------------------------------------------

//! \return human-readable name of Tree Function.
const char* exeCSG_RectHoleFunc::GetName() const
{
  return "Rectangular hole self-evaluation";
}

//-----------------------------------------------------------------------------

//! Executes Tree Function.
//! \param argsIn   [in] INPUT Parameters.
//! \param argsOut  [in] OUTPUT Parameters
//! \param userData [in] user data.
//! \param PEntry   [in] Progress Entry.
//! \return execution status.
int exeCSG_RectHoleFunc::execute(const Handle(ActAPI_HParameterList)& argsIn,
                                 const Handle(ActAPI_HParameterList)& argsOut,
                                 const Handle(Standard_Transient)&    ActData_NotUsed(userData),
                                 ActAPI_ProgressEntry                 PEntry) const
{
  /* ============================
   *  Interpret INPUT Parameters
   * ============================ */

  // TODO: NYI

  /* =============================
   *  Interpret OUTPUT Parameters
   * ============================= */

  // TODO: NYI

  /* =================
   *  Construct shape
   * ================= */

  // TODO: NYI

  /* ==================================
   *  Set results to OUTPUT Parameters
   * ================================== */

  // TODO: NYI

  // Put logging message
  PEntry.SendLogMessage(LogInfo(Normal) << "Rectangular hole was self-evaluated.");

  return 0; // OK
}

//-----------------------------------------------------------------------------

//! Returns accepted INPUT signature for VALIDATION.
//! \return expected INPUT signature.
ActAPI_ParameterTypeStream exeCSG_RectHoleFunc::inputSignature() const
{
  return ActAPI_ParameterTypeStream() << Parameter_Real  // O_x
                                      << Parameter_Real  // O_y
                                      << Parameter_Real  // O_z
                                      << Parameter_Real  // n_x
                                      << Parameter_Real  // n_y
                                      << Parameter_Real  // n_z
                                      << Parameter_Real  // Ksi_x
                                      << Parameter_Real  // Ksi_y
                                      << Parameter_Real  // Ksi_z
                                      << Parameter_Real  // Width
                                      << Parameter_Real  // Height
                                      << Parameter_Real; // Depth
}

//-----------------------------------------------------------------------------

//! Returns accepted OUTPUT signature for VALIDATION.
//! \return expected OUTPUT signature.
ActAPI_ParameterTypeStream exeCSG_RectHoleFunc::outputSignature() const
{
  return ActAPI_ParameterTypeStream() << Parameter_Shape;
}
