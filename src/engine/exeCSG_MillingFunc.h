//-----------------------------------------------------------------------------
// Created on: 26 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_MillingFunc_h
#define exeCSG_MillingFunc_h

// exeCSG includes
#include <exeCSG_FeatureFacesParameter.h>

// Active Data includes
#include <ActData_BaseTreeFunction.h>

//! Tree Function for self-evaluation of a stock model.
//!
//! <pre>
//! INPUT Parameters:
//!     +=============+
//!     | Stock shape |
//!     +=============+
//!     +=================+     +=================+
//! --> | Feature shape 1 | --> | Feature shape 2 | --> ...
//!     +=================+     +=================+
//!
//!                              ||
//!                              ||
//!                             _||_  EXECUTION
//!                             \  /
//!                              \/
//!
//! OUTPUT Parameters:
//!      +==========================================+
//!      | Workpie�e shape after milling simulation |
//!      +==========================================+
//! </pre>
class exeCSG_MillingFunc : public ActData_BaseTreeFunction
{
public:

  // OCCT RTTI
  DEFINE_STANDARD_RTTI_INLINE(exeCSG_MillingFunc, ActData_BaseTreeFunction)

public:

  static Handle(exeCSG_MillingFunc)
    Instance();

  static const char*
    GUID();

  virtual const char*
    GetGUID() const;

  virtual const char*
    GetName() const;

public:

  //! Returns true if this Tree Function is HEAVY, false -- otherwise.
  //! \return always false.
  virtual bool IsHeavy() const
  {
    return false;
  }

private:

  virtual int
    execute(const Handle(ActAPI_HParameterList)& argsIn,
            const Handle(ActAPI_HParameterList)& argsOut,
            const Handle(Standard_Transient)&    userData = NULL,
            ActAPI_ProgressEntry                 PEntry = NULL) const;

  virtual bool
    validateInput(const Handle(ActAPI_HParameterList)& argsIn) const;

  virtual ActAPI_ParameterTypeStream
    inputSignature() const;

  virtual ActAPI_ParameterTypeStream
    outputSignature() const;

protected:

  //! Populates (with initial clearing) the Parameter which stores feature
  //! faces and their corresponding feature IDs.
  //! \param[in] param        Parameter which stores feature faces distributed by
  //!                         persistent feature IDs.
  //! \param[in] featureFaces feature faces to store.
  void repopulateFeatureFaces(const Handle(exeCSG_FeatureFacesParameter)& param,
                              const Handle(exeCSG_FeatureFaces)&          featureFaces) const;

  //! Checks whether the given feature face is isolated wrt the support face.
  //! \param[in] featureFace feature face to check.
  //! \param[in] supportFace support face.
  //! \return true/false.
  bool isIsolatedInSupportFace(const TopoDS_Face& featureFace,
                               const TopoDS_Face& supportFace) const;

  //! Finds support faces of the workpiece.
  //! \param[in]  workpiece    workpiece model.
  //! \param[out] supportFaces found support faces.
  void findSupportFaces(const TopoDS_Shape&   workpiece,
                        TopTools_ListOfShape& supportFaces) const;

  bool isFeatureIsolated(const TopTools_ListOfShape& featureFaces,
                         const TopTools_ListOfShape& supportFaces) const;

private:

  exeCSG_MillingFunc() : ActData_BaseTreeFunction() {}

};

#endif
