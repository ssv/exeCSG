//-----------------------------------------------------------------------------
// Created on: 22 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_Model_h
#define exeCSG_Model_h

// exeCSG includes

#include <exeCSG_DrillHoleNode.h>
#include <exeCSG_RectHoleNode.h>
#include <exeCSG_StockNode.h>
#include <exeCSG_WorkpieceNode.h>

// asiEngine includes
#include <asiEngine_Model.h>

//-----------------------------------------------------------------------------

//! Data Model for CSG demo.
class exeCSG_Model : public asiEngine_Model
{
public:

  // OCCT RTTI
  DEFINE_STANDARD_RTTI_INLINE(exeCSG_Model, asiEngine_Model)

public:

  exeCSG_Model();

//-----------------------------------------------------------------------------
// Populate and clear:
public:

  virtual void
    Populate();

  virtual void
    Clear();

//-----------------------------------------------------------------------------
// Accessors to Nodes:
public:

  Handle(exeCSG_StockNode)
    GetStockNode() const;

  Handle(exeCSG_WorkpieceNode)
    GetWorkpieceNode() const;

//-----------------------------------------------------------------------------
// Overridden:
public:

  //! Create a cloned instance of Data Model.
  //! \return cloned instance.
  virtual Handle(ActAPI_IModel) Clone() const
  {
    return ActData_BaseModel::CloneInstance<exeCSG_Model>();
  }

//-----------------------------------------------------------------------------
// Partitions:
public:

  //! \return requested Partition.
  Handle(asiData_Partition<exeCSG_DrillHoleNode>) GetDrillHolePartition() const
  {
    return Handle(asiData_Partition<exeCSG_DrillHoleNode>)::DownCast( this->Partition(Partition_DrillHole) );
  }

  //! \return requested Partition.
  Handle(asiData_Partition<exeCSG_RectHoleNode>) GetRectHolePartition() const
  {
    return Handle(asiData_Partition<exeCSG_RectHoleNode>)::DownCast( this->Partition(Partition_RectHole) );
  }

  //! \return requested Partition.
  Handle(asiData_Partition<exeCSG_StockNode>) GetStockPartition() const
  {
    return Handle(asiData_Partition<exeCSG_StockNode>)::DownCast( this->Partition(Partition_Stock) );
  }

  //! \return requested Partition.
  Handle(asiData_Partition<exeCSG_WorkpieceNode>) GetWorkpiecePartition() const
  {
    return Handle(asiData_Partition<exeCSG_WorkpieceNode>)::DownCast( this->Partition(Partition_Workpiece) );
  }

private:

  virtual void
    initPartitions();

  virtual void
    initFunctionDrivers();

private:

  //! IDs of the registered Partitions.
  enum PartitionId
  {
    Partition_Stock = Partition_LAST,
    Partition_DrillHole,
    Partition_RectHole,
    Partition_Workpiece
  };

};

#endif
