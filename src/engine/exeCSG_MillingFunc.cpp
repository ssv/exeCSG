//-----------------------------------------------------------------------------
// Created on: 26 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_MillingFunc.h>

// exeCSG includes
#include <exeCSG_CommonFacilities.h>

// asiAlgo includes
#include <asiAlgo_Timer.h>
#include <asiAlgo_SuppressFaces.h>
#include <asiAlgo_Utils.h>

// Active Data includes
#include <ActData_ParameterFactory.h>

// OCCT includes
#include <BOPCol_DataMapOfShapeListOfShape.hxx>
#include <BRep_Builder.hxx>
#include <BRepAlgoAPI_Cut.hxx>
#include <Geom_Circle.hxx>
#include <ShapeAnalysis.hxx>
#include <TopExp.hxx>
#include <TopExp_Explorer.hxx>
#include <TopoDS.hxx>

#define ENABLE_OPTIMIZED_CUT

#undef COUT_DEBUG
#if defined COUT_DEBUG
  #pragma message("===== warning: COUT_DEBUG is enabled")
#endif

//-----------------------------------------------------------------------------

Quantity_Color GetColorForId(const ActAPI_DataObjectId& id)
{
  if ( id == "0:2:30:1" )
    return Color_Red;

  if ( id == "0:2:30:2" )
    return Color_Green;

  if ( id == "0:2:30:3" )
    return Color_Blue;

  if ( id == "0:2:30:4" )
    return Color_Yellow;

  return Color_White;
}

//-----------------------------------------------------------------------------

void GetToolFaces(const TopoDS_Shape&   Tool,
                  TopTools_ListOfShape& Faces2Trace)
{
  for ( TopExp_Explorer exp(Tool, TopAbs_FACE); exp.More(); exp.Next() )
  {
    const TopoDS_Face& currentFace = TopoDS::Face( exp.Current() );
    Faces2Trace.Append(currentFace);
  }
}

//-----------------------------------------------------------------------------

//! \return Tree Function instance.
Handle(exeCSG_MillingFunc) exeCSG_MillingFunc::Instance()
{
  return new exeCSG_MillingFunc();
}

//-----------------------------------------------------------------------------

//! \return GUID.
const char* exeCSG_MillingFunc::GUID()
{
  return "74D69C86-F225-44C3-A83E-EC66D462CB5D";
}

//-----------------------------------------------------------------------------

//! \return GUID.
const char* exeCSG_MillingFunc::GetGUID() const
{
  return GUID();
}

//-----------------------------------------------------------------------------

//! \return human-readable name of Tree Function.
const char* exeCSG_MillingFunc::GetName() const
{
  return "Milling";
}

//-----------------------------------------------------------------------------

//! Executes Tree Function.
//! \param argsIn   [in] INPUT Parameters.
//! \param argsOut  [in] OUTPUT Parameters
//! \param userData [in] user data.
//! \param PEntry   [in] Progress Entry.
//! \return execution status.
int exeCSG_MillingFunc::execute(const Handle(ActAPI_HParameterList)& argsIn,
                                const Handle(ActAPI_HParameterList)& argsOut,
                                const Handle(Standard_Transient)&    ActData_NotUsed(userData),
                                ActAPI_ProgressEntry                 PEntry) const
{
  Handle(exeCSG_CommonFacilities) cf = exeCSG_CommonFacilities::Instance();

  /* ============================
   *  Interpret INPUT Parameters
   * ============================ */

  // First Parameter is a stock shape
  ActAPI_HParameterList::Iterator pit(*argsIn);
  Handle(ActAPI_IUserParameter) stockParameter = pit.Value();
  //
  TopoDS_Shape stock = ActParamTool::AsShape(stockParameter)->GetShape();
  //
  pit.Next();

  // This map stores correspondence between data object IDs and feature faces
  Handle(exeCSG_FeatureFaces) faces2Trace = new exeCSG_FeatureFaces;

  // Then tools go
  NCollection_Map<ActAPI_DataObjectId> modifiedFeaturesMap;
  ActAPI_DataObjectIdList              modifiedFeatures;
  std::vector<int>                     argForModifiedFeatures;
  //
  TopTools_ListOfShape tools;
  //
  int argIndex = 2;
  for ( ; pit.More(); pit.Next(), ++argIndex )
  {
    Handle(ActAPI_IUserParameter) param = pit.Value();

    // Feature ID is the ID of the persistent object
    ActAPI_DataObjectId featureId = param->GetNode()->GetId();

    // Get shape
    TopoDS_Shape tool = ActParamTool::AsShape(param)->GetShape();
    //
    if ( tool.IsNull() )
      continue;

    // Add tool to the collection of tools
    tools.Append(tool);

    // Get tool faces we want to track history for
    TopTools_ListOfShape featureFaces;
    GetToolFaces(tool, featureFaces);
    //
    if ( !featureFaces.IsEmpty() )
      faces2Trace->ChangeMap().Bind(featureId, featureFaces);

    // Find out which features were actually modified
    if ( ActData_LogBook::IsModifiedCursor(param) )
    {
      modifiedFeaturesMap.Add(featureId);
      modifiedFeatures.Append(featureId);
      argForModifiedFeatures.push_back(argIndex);
    }
  }

  // Dump modified features
  if ( ActData_LogBook::IsModifiedCursor(stockParameter) )
  {
    std::cout << ">>> Modified stock" << stockParameter->GetNode()->GetId().ToCString() << std::endl;
  }
  //
  for ( ActAPI_DataObjectIdList::Iterator it(modifiedFeatures); it.More(); it.Next() )
  {
    std::cout << ">>> Modified feature: " << it.Value().ToCString() << std::endl;
  }

  /* =============================
   *  Interpret OUTPUT Parameters
   * ============================= */

  Handle(ActData_ShapeParameter)
    outParamWorkpiece = ActParamTool::AsShape( argsOut->Value(1) );

  // Get workpiece
  TopoDS_Shape workpiece = outParamWorkpiece->GetShape();

  /* ===========================
   *  Construct workpiece shape
   * =========================== */

  // Check if the feature faces corresponding to the modified features are isolated
  // ...

  // Get Feature Faces Parameter
  Handle(ActAPI_INode) N = outParamWorkpiece->GetNode();
  //
  Handle(exeCSG_FeatureFacesParameter)
    paramFeatureFaces = Handle(exeCSG_FeatureFacesParameter)::DownCast( N->Parameter(exeCSG_WorkpieceNode::PID_FeatureFaces) );

  bool isFullUpdateRequired = true;

#if defined ENABLE_OPTIMIZED_CUT

  // Get support faces of the workpiece
  TopTools_ListOfShape supportFaces;
  this->findSupportFaces(workpiece, supportFaces);

  // For each modified feature, check if it isolated or not
  bool areFeaturesSoft = true;
  //
  for ( ActAPI_DataObjectIdList::Iterator it(modifiedFeatures); it.More(); it.Next() )
  {
    const ActAPI_DataObjectId& featureId = it.Value();

    // Get feature faces which are currently present in a stock
    TopTools_ListOfShape featureFaces;
    //
    if ( !paramFeatureFaces->GetFeatureFaces()->Get(featureId, featureFaces) )
    {
      areFeaturesSoft = false;
      break;
    }

    // Check if a feature is isolated
    if ( !this->isFeatureIsolated(featureFaces, supportFaces) )
    {
      areFeaturesSoft = false;
      break;
    }
  }
  if ( areFeaturesSoft )
    isFullUpdateRequired = false;

  if ( isFullUpdateRequired )
    cf->ProgressNotifier->SendLogMessage(LogInfo(Normal) << "FULL update" );
  else
    cf->ProgressNotifier->SendLogMessage(LogInfo(Normal) << "INDIVIDUAL update" );

  if ( !isFullUpdateRequired )
  {
    // Re-initialize tools
    tools.Clear();
    //
    TopTools_ListOfShape faces2Suppress;
    //
    int k = 0;
    for ( ActAPI_DataObjectIdList::Iterator it(modifiedFeatures); it.More(); it.Next(), ++k )
    {
      const ActAPI_DataObjectId& featureId = it.Value();

      // Get feature faces which are currently present in a stock
      TopTools_ListOfShape featureFaces;
      //
      paramFeatureFaces->GetFeatureFaces()->Get(featureId, featureFaces);

      // Fill the list of faces to suppress
      faces2Suppress.Append(featureFaces);

      // Add tool
      tools.Append( ActParamTool::AsShape( argsIn->Value(argForModifiedFeatures[k]) )->GetShape() );
    }

    // Suppress
    asiAlgo_SuppressFaces Suppressor(workpiece);
    if ( !Suppressor.Perform(faces2Suppress, false) )
    {
      std::cout << "Error: failed to suppress faces" << std::endl;
      return 1;
    }
    workpiece = Suppressor.GetResult();
  }
#endif

  TIMER_NEW
  TIMER_GO

  BRepAlgoAPI_Cut API;
  //
  workpiece = asiAlgo_Utils::BooleanCut(isFullUpdateRequired ? stock : workpiece,
                                        tools,
                                        true,
                                        0,
                                        API);

  TIMER_FINISH
  TIMER_COUT_RESULT_MSG("Boolean cut")

  // Loop over the map of feature faces
  cf->PlotterWorkpiece->ERASE_ALL();
  //
  Handle(exeCSG_FeatureFaces) featureImageFaces;
  //
  if ( isFullUpdateRequired )
  {
    featureImageFaces = new exeCSG_FeatureFaces;
    //
    for ( exeCSG_FeatureFaces::t_feature_faces::Iterator mit( faces2Trace->Map() ); mit.More(); mit.Next() )
    {
      const ActAPI_DataObjectId&  featureId    = mit.Key();
      const TopTools_ListOfShape& featureFaces = mit.Value();

      std::cout << featureId.ToCString() << std::endl;

      // Ask for image for every face
      for ( TopTools_ListIteratorOfListOfShape lit(featureFaces); lit.More(); lit.Next() )
      {
        const TopoDS_Shape& featureFace = lit.Value();

        TopoDS_Shape image = asiAlgo_Utils::GetImage(featureFace, API);


#if defined COUT_DEBUG
        if ( !image.IsNull() && !asiAlgo_Utils::Contains(workpiece, featureFace) )
        {
          std::cout << "!!! Workpiece does not contain " << asiAlgo_Utils::ShapeAddrWithPrefix(featureFace) << std::endl;
        }
#endif

        if ( image.IsNull() )
        {

#if defined COUT_DEBUG
          std::cout << "\t"   << asiAlgo_Utils::ShapeAddrWithPrefix(featureFace)
                    << " -> " << "NULL" << std::endl;
#endif

          continue; // The shape has null image (it has been DELETED)
        }

        // Add to images
        featureImageFaces->Add(featureId, image);

#if defined COUT_DEBUG
        std::cout << "\t"   << asiAlgo_Utils::ShapeAddrWithPrefix(featureFace)
                  << " -> " << asiAlgo_Utils::ShapeAddrWithPrefix(image) << std::endl;
#endif

        cf->PlotterWorkpiece->DRAW_SHAPE(image, GetColorForId(featureId), "image");
      }
    }
  }
  else
  {
    featureImageFaces = paramFeatureFaces->GetFeatureFaces();

    // Update history of features which were not re-evaluated
    exeCSG_FeatureFaces::t_feature_faces map = featureImageFaces->Map();
    for ( exeCSG_FeatureFaces::t_feature_faces::Iterator mit(map); mit.More(); mit.Next() )
    {
      ActAPI_DataObjectId featureId = mit.Key();

      if ( modifiedFeaturesMap.Contains(featureId) )
        continue;

      const TopTools_ListOfShape& featureFaces = mit.Value();

      std::cout << featureId.ToCString() << std::endl;

      // Ask for image for every face
      TopTools_ListOfShape updatedFeatureFaces;
      //
      for ( TopTools_ListIteratorOfListOfShape lit(featureFaces); lit.More(); lit.Next() )
      {
        const TopoDS_Shape& featureFace = lit.Value();

        TopoDS_Shape image = asiAlgo_Utils::GetImage(featureFace, API);

        if ( image.IsNull() )
        {
#if defined COUT_DEBUG
          std::cout << "\t"   << asiAlgo_Utils::ShapeAddrWithPrefix(featureFace)
                    << " -> " << "NULL" << std::endl;
#endif

          continue; // The shape has null image (it has been DELETED)
        }

        // Update
        if ( image.ShapeType() == TopAbs_COMPOUND ) // For multiple images in history
        {
          for ( TopExp_Explorer exp(image, TopAbs_FACE); exp.More(); exp.Next() )
            updatedFeatureFaces.Append( exp.Current() );
        }
        else
          updatedFeatureFaces.Append(image);

#if defined COUT_DEBUG
        std::cout << "\t"   << asiAlgo_Utils::ShapeAddrWithPrefix(featureFace)
                  << " -> " << asiAlgo_Utils::ShapeAddrWithPrefix(image) << std::endl;
#endif

        cf->PlotterWorkpiece->DRAW_SHAPE(image, GetColorForId(featureId), "image");
      }

      featureImageFaces->ChangeMap().UnBind(featureId);
      featureImageFaces->ChangeMap().Bind(featureId, updatedFeatureFaces);
    }

    // Update history of features which were re-evaluated
    for ( ActAPI_DataObjectIdList::Iterator fit(modifiedFeatures); fit.More(); fit.Next() )
    {
      ActAPI_DataObjectId featureId = fit.Value();

      const TopTools_ListOfShape& featureFaces = faces2Trace->Map()(featureId);

      std::cout << featureId.ToCString() << std::endl;

      // Ask for image for every face
      TopTools_ListOfShape updatedFeatureFaces;
      //
      for ( TopTools_ListIteratorOfListOfShape lit(featureFaces); lit.More(); lit.Next() )
      {
        const TopoDS_Shape& featureFace = lit.Value();

        TopoDS_Shape image = asiAlgo_Utils::GetImage(featureFace, API);

        if ( image.IsNull() )
        {
#if defined COUT_DEBUG
          std::cout << "\t"   << asiAlgo_Utils::ShapeAddrWithPrefix(featureFace)
                    << " -> " << "NULL" << std::endl;
#endif

          continue; // The shape has null image (it has been DELETED)
        }

        // Update
        if ( image.ShapeType() == TopAbs_COMPOUND ) // For multiple images in history
        {
          for ( TopExp_Explorer exp(image, TopAbs_FACE); exp.More(); exp.Next() )
            updatedFeatureFaces.Append( exp.Current() );
        }
        else
          updatedFeatureFaces.Append(image);

#if defined COUT_DEBUG
        std::cout << "\t"   << asiAlgo_Utils::ShapeAddrWithPrefix(featureFace)
                  << " -> " << asiAlgo_Utils::ShapeAddrWithPrefix(image) << std::endl;
#endif

        cf->PlotterWorkpiece->DRAW_SHAPE(image, GetColorForId(featureId), "image");
      }

      featureImageFaces->ChangeMap().UnBind(featureId);
      featureImageFaces->ChangeMap().Bind(featureId, updatedFeatureFaces);
    }
  }

  // Store images of features
  this->repopulateFeatureFaces(paramFeatureFaces, featureImageFaces);

  /* ==================================
   *  Set results to OUTPUT Parameters
   * ================================== */

  // Set output
  outParamWorkpiece->SetShape(workpiece, MT_Impacted);

  // Put logging message
  PEntry.SendLogMessage(LogInfo(Normal) << "Milling was done successfully.");

  return 0; // OK
}

//-----------------------------------------------------------------------------

//! Not used.
ActAPI_ParameterTypeStream exeCSG_MillingFunc::inputSignature() const
{
  return ActAPI_ParameterTypeStream();
}

//-----------------------------------------------------------------------------

//! Returns accepted OUTPUT signature for VALIDATION.
//! \return expected OUTPUT signature.
ActAPI_ParameterTypeStream exeCSG_MillingFunc::outputSignature() const
{
  return ActAPI_ParameterTypeStream() << Parameter_Shape;
}

//-----------------------------------------------------------------------------

bool exeCSG_MillingFunc::validateInput(const Handle(ActAPI_HParameterList)& argsIn) const
{
  for ( ActAPI_HParameterList::Iterator pit(*argsIn); pit.More(); pit.Next() )
  {
    if ( !pit.Value()->IsInstance( STANDARD_TYPE(ActData_ShapeParameter) ) )
      return false;
  }
  return true;
}

//-----------------------------------------------------------------------------

void exeCSG_MillingFunc::repopulateFeatureFaces(const Handle(exeCSG_FeatureFacesParameter)& param,
                                                const Handle(exeCSG_FeatureFaces)&          featureFaces) const
{
  param->SetFeatureFaces(featureFaces);
}

//------------------------------------------------------------------------------

bool
  exeCSG_MillingFunc::isIsolatedInSupportFace(const TopoDS_Face& featureFace,
                                              const TopoDS_Face& supportFace) const
{
  // CAUTION: the following check is not enough. If common edges of a feature
  //          belong to the inner wire of a support face, this does not mean
  //          that a feature is isolated. It may happen that inner contour
  //          is simply bigger and contains all common edges in a bridge face.

  // Extract common edges for faces
  TopTools_IndexedMapOfShape EdgesFeature, EdgesMaster, EdgesCommon;
  TopExp::MapShapes(featureFace, TopAbs_EDGE, EdgesFeature);
  TopExp::MapShapes(supportFace, TopAbs_EDGE, EdgesMaster);
  //
  for ( int ef = 1; ef <= EdgesFeature.Extent(); ++ef )
  {
    for ( int eg = 1; eg <= EdgesMaster.Extent(); ++eg )
    {
      if ( EdgesFeature(ef).IsSame( EdgesMaster(eg) ) )
        EdgesCommon.Add( EdgesFeature(ef) );
    }
  }

  if ( EdgesCommon.IsEmpty() )
    return true; // No common edges, so let's return ISOLATED (true)

  // Soft insertion is realized if all common edges are used in the
  // internal wires of master face
  TopoDS_Wire masterOuterWire = ShapeAnalysis::OuterWire(supportFace);
  TopTools_IndexedMapOfShape outerWireVert;
  TopExp::MapShapes(masterOuterWire, TopAbs_VERTEX, outerWireVert);
  //
  for ( int e = 1; e <= EdgesCommon.Extent(); ++e )
  {
    const TopoDS_Shape& featureEdge = EdgesCommon(e);

    // Try to find this edge among internal wires of the master face
    bool isFound = false;
    for ( TopoDS_Iterator wit(supportFace); wit.More(); wit.Next() )
    {
      if ( wit.Value().ShapeType() != TopAbs_WIRE || wit.Value().IsPartner(masterOuterWire) )
        continue; // Skip strange topologies and outer wire

      // Take inner wire
      const TopoDS_Wire& masterInnerWire = TopoDS::Wire( wit.Value() );

      for ( TopoDS_Iterator eit(masterInnerWire); eit.More(); eit.Next() )
      {
        if ( eit.Value().ShapeType() != TopAbs_EDGE )
          continue;

        if ( eit.Value().IsPartner(featureEdge) )
        {
          isFound = true;
          break;
        }
      }

      ///
      double f, l;
      Handle(Geom_Curve) curve = BRep_Tool::Curve(TopoDS::Edge(featureEdge), f, l);
      //
      if ( curve->IsInstance( STANDARD_TYPE(Geom_Circle) ) )
      {
        BRep_Tool::Range(TopoDS::Edge(featureEdge), f, l);

        if ( Abs(l - f) < 2*M_PI - 0.0001 )
          return false;
      }

      // Check that there is no intersection between inner and outer wire.
      // If such intersection is found, "hard" state will be returned.
      TopTools_IndexedMapOfShape innerWireVert;
      TopExp::MapShapes(masterInnerWire, TopAbs_VERTEX, innerWireVert);
      TopTools_IndexedMapOfShape::Iterator innerVertIter(innerWireVert);
      for ( ; innerVertIter.More() ; innerVertIter.Next())
      {
        if ( outerWireVert.Contains( innerVertIter.Value() ) )
        {
          return false;
        }
      }
      if ( isFound ) break;
    }

    if ( !isFound )
      return false;
  }

  return true;
}

//-----------------------------------------------------------------------------

void exeCSG_MillingFunc::findSupportFaces(const TopoDS_Shape&   workpiece,
                                          TopTools_ListOfShape& supportFaces) const
{
  // WARNING: this method is very limited. The simplified check it uses is
  //          not enough at all!!!

  for ( TopExp_Explorer exp(workpiece, TopAbs_FACE); exp.More(); exp.Next() )
  {
    const TopoDS_Shape& currentFace = exp.Current();

    // Check how many wires are in a face and choose those with several wires
    TopTools_IndexedMapOfShape MapOfWires;
    TopExp::MapShapes(currentFace, TopAbs_WIRE, MapOfWires);
    //
    if ( MapOfWires.Extent() > 1 )
      supportFaces.Append(currentFace);
  }
}

//-----------------------------------------------------------------------------

bool exeCSG_MillingFunc::isFeatureIsolated(const TopTools_ListOfShape& featureFaces,
                                           const TopTools_ListOfShape& supportFaces) const
{
  // Find BRIDGE faces. A BRIDGE face is a feature face which is
  // connected to a support face.
  for ( TopTools_ListIteratorOfListOfShape sit(supportFaces); sit.More(); sit.Next() ) 
  {
    const TopoDS_Face& supportFace = TopoDS::Face( sit.Value() );

    // Loop over the feature faces
    for ( TopTools_ListIteratorOfListOfShape fit(featureFaces); fit.More(); fit.Next() ) 
    {
      const TopoDS_Face& featureFace = TopoDS::Face( fit.Value() );

      if ( !this->isIsolatedInSupportFace(featureFace, supportFace) )
        return false;
    }
  }

  return true;
}
