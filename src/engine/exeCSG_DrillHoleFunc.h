//-----------------------------------------------------------------------------
// Created on: 26 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_DrillHoleFunc_h
#define exeCSG_DrillHoleFunc_h

// Active Data includes
#include <ActData_BaseTreeFunction.h>

//! Tree Function for self-evaluation of a drill hole feature.
class exeCSG_DrillHoleFunc : public ActData_BaseTreeFunction
{
public:

  // OCCT RTTI
  DEFINE_STANDARD_RTTI_INLINE(exeCSG_DrillHoleFunc, ActData_BaseTreeFunction)

public:

  static Handle(exeCSG_DrillHoleFunc)
    Instance();

  static const char*
    GUID();

  virtual const char*
    GetGUID() const;

  virtual const char*
    GetName() const;

public:

  //! Returns true if this Tree Function is HEAVY, false -- otherwise.
  //! \return always false.
  virtual bool IsHeavy() const
  {
    return false;
  }

private:

  virtual int
    execute(const Handle(ActAPI_HParameterList)& argsIn,
            const Handle(ActAPI_HParameterList)& argsOut,
            const Handle(Standard_Transient)&    userData = NULL,
            ActAPI_ProgressEntry                 PEntry = NULL) const;

  virtual ActAPI_ParameterTypeStream
    inputSignature() const;

  virtual ActAPI_ParameterTypeStream
    outputSignature() const;

private:

  exeCSG_DrillHoleFunc() : ActData_BaseTreeFunction() {}

};

#endif
