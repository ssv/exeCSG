//-----------------------------------------------------------------------------
// Created on: 22 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_Model.h>

// exeCSG includes
#include <exeCSG_Commands.h>
#include <exeCSG_DrillHoleFunc.h>
#include <exeCSG_MillingFunc.h>
#include <exeCSG_RectHoleFunc.h>
#include <exeCSG_StockFunc.h>

// asiEngine includes
#include <asiEngine_IV.h>

//-----------------------------------------------------------------------------
// Register Node types
//-----------------------------------------------------------------------------

REGISTER_NODE_TYPE(exeCSG_DrillHoleNode)
REGISTER_NODE_TYPE(exeCSG_RectHoleNode)
REGISTER_NODE_TYPE(exeCSG_StockNode)
REGISTER_NODE_TYPE(exeCSG_WorkpieceNode)

//-----------------------------------------------------------------------------

//! Default constructor.
exeCSG_Model::exeCSG_Model() : asiEngine_Model()
{}

//-----------------------------------------------------------------------------

//! Populates Data Model.
void exeCSG_Model::Populate()
{
  // Add root Node
  Handle(asiData_RootNode)
    root_n = Handle(asiData_RootNode)::DownCast( asiData_RootNode::Instance() );
  //
  this->GetRootPartition()->AddNode(root_n);

  // Set name
  root_n->SetName("CSG modeling");

  // Create a tool for business logic
  exeCSG_Commands API(this);

  // Create Stock and Workpiece Nodes
  API.CreateStockNode();
  API.CreateWorkpieceNode();

  // Add Imperative Viewer Node
  root_n->AddChildNode( asiEngine_IV(this).Create_IV() );
}

//-----------------------------------------------------------------------------

//! Clears the Model.
void exeCSG_Model::Clear()
{}

//-----------------------------------------------------------------------------

//! \return single Stock Node.
Handle(exeCSG_StockNode) exeCSG_Model::GetStockNode() const
{
  for ( ActData_BasePartition::Iterator it( this->GetStockPartition() ); it.More(); it.Next() )
  {
    Handle(exeCSG_StockNode) N = Handle(exeCSG_StockNode)::DownCast( it.Value() );
    //
    if ( !N.IsNull() && N->IsWellFormed() )
      return N;
  }
  return NULL;
}

//-----------------------------------------------------------------------------

//! \return single Workpiece Node.
Handle(exeCSG_WorkpieceNode) exeCSG_Model::GetWorkpieceNode() const
{
  for ( ActData_BasePartition::Iterator it( this->GetWorkpiecePartition() ); it.More(); it.Next() )
  {
    Handle(exeCSG_WorkpieceNode) N = Handle(exeCSG_WorkpieceNode)::DownCast( it.Value() );
    //
    if ( !N.IsNull() && N->IsWellFormed() )
      return N;
  }
  return NULL;
}

//-----------------------------------------------------------------------------

//! Initializes Partitions.
void exeCSG_Model::initPartitions()
{
  // Register basic Partitions
  asiEngine_Model::initPartitions();

  // Register Partitions
  REGISTER_PARTITION(asiData_Partition<exeCSG_StockNode>,     Partition_Stock);
  REGISTER_PARTITION(asiData_Partition<exeCSG_DrillHoleNode>, Partition_DrillHole);
  REGISTER_PARTITION(asiData_Partition<exeCSG_RectHoleNode>,  Partition_RectHole);
  REGISTER_PARTITION(asiData_Partition<exeCSG_WorkpieceNode>, Partition_Workpiece);
}

//-----------------------------------------------------------------------------

//! Registers Tree Functions.
void exeCSG_Model::initFunctionDrivers()
{
  REGISTER_TREE_FUNCTION(exeCSG_DrillHoleFunc);
  REGISTER_TREE_FUNCTION(exeCSG_MillingFunc);
  REGISTER_TREE_FUNCTION(exeCSG_RectHoleFunc);
  REGISTER_TREE_FUNCTION(exeCSG_StockFunc);
}
