//-----------------------------------------------------------------------------
// Created on: 29 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_FeatureFaces_h
#define exeCSG_FeatureFaces_h

// exeCSG includes
#include <exeCSG.h>

// OCCT includes
#include <NCollection_DataMap.hxx>
#include <Standard_Type.hxx>
#include <TopExp_Explorer.hxx>
#include <TopTools_ListOfShape.hxx>

//! Feature faces.
class exeCSG_FeatureFaces : public Standard_Transient
{
public:

  // OCCT RTTI
  DEFINE_STANDARD_RTTI_INLINE(exeCSG_FeatureFaces, Standard_Transient)

public:

  //! Convenience type definition for feature IDs (given that a feature is
  //! a data object in OCAF) associated with their explicitly defined faces.
  typedef NCollection_DataMap<ActAPI_DataObjectId, TopTools_ListOfShape> t_feature_faces;

public:

  //! Clears the collection of feature faces.
  void Clear()
  {
    m_data.Clear();
  }

  //! \return true if no feature faces are available.
  bool IsEmpty() const
  {
    return m_data.IsEmpty();
  }

  //! Checks whether the passed feature is recorded.
  //! \param featureId [in] feature ID to check.
  //! \return true/false.
  bool Has(const ActAPI_DataObjectId& featureId) const
  {
    return m_data.IsBound(featureId);
  }

  //! Adds a feature face for the given feature.
  //! \param featureId [in] ID of the feature to add a face for.
  //! \param face      [in] face to add.
  void Add(const ActAPI_DataObjectId& featureId,
           const TopoDS_Shape&        face)
  {
    if ( !m_data.IsBound(featureId) )
      m_data.Bind( featureId, TopTools_ListOfShape() );

    if ( face.ShapeType() == TopAbs_COMPOUND ) // For multiple images in history
    {
      for ( TopExp_Explorer exp(face, TopAbs_FACE); exp.More(); exp.Next() )
        m_data(featureId).Append( exp.Current() );
    }
    else
      m_data(featureId).Append(face);
  }

  //! Returns the list of feature faces for the given feature ID.
  //! \param featureId [in]  ID of the feature.
  //! \param faces     [out] feature faces.
  //! \return false if no feature faces are stored for the passed ID.
  bool Get(const ActAPI_DataObjectId& featureId,
           TopTools_ListOfShape&      faces)
  {
    if ( !m_data.IsBound(featureId) )
      return false;

    faces = m_data(featureId);
    return true;
  }

  //! \return map for direct modifications.
  t_feature_faces& ChangeMap() { return m_data; }

  //! \return map for read-only access.
  const t_feature_faces& Map() const { return m_data; }

protected:

  t_feature_faces m_data; //!< Feature IDs and the corresponnding faces.

};

#endif
