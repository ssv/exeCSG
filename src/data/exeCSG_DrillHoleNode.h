//-----------------------------------------------------------------------------
// Created on: 22 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_DrillHoleNode_h
#define exeCSG_DrillHoleNode_h

// exeCSG includes
#include <exeCSG_FeatureNode.h>

//! Drill hole feature.
class exeCSG_DrillHoleNode : public exeCSG_FeatureNode
{
public:

  // OCCT RTTI
  DEFINE_STANDARD_RTTI_INLINE(exeCSG_DrillHoleNode, exeCSG_FeatureNode)

  // Automatic registration of Node type in global factory
  DEFINE_NODE_FACTORY(exeCSG_DrillHoleNode, Instance)

public:

  //! IDs for the underlying Parameters.
  enum ParamId
  {
    PID_Radius = PID_FeatureNodeLast,
    PID_Depth,
  //-------------------//
    PID_Shape,         //!< Evaluated feature shape.
  //-------------------//
    PID_FuncSelfEval,  //!< Self-evaluation Tree Function.
  //-------------------//
    PID_DrillHoleLast
  };

public:

  static Handle(ActAPI_INode) Instance();

// Initialization:
public:

  void Init(const gp_Ax3& placement,
            const double  radius,
            const double  depth);

// Convenience methods:
public:

  //! Sets radius value.
  //! \param[in] val value to set.
  void SetRadius(const double val)
  {
    ActParamTool::AsReal( this->Parameter(PID_Radius) )->SetValue(val);
  }

  //! \return radius value.
  double GetRadius() const
  {
    return ActParamTool::AsReal( this->Parameter(PID_Radius) )->GetValue();
  }

  //! Sets depth value.
  //! \param[in] val value to set.
  void SetDepth(const double val)
  {
    ActParamTool::AsReal( this->Parameter(PID_Depth) )->SetValue(val);
  }

  //! \return depth value.
  double GetDepth() const
  {
    return ActParamTool::AsReal( this->Parameter(PID_Depth) )->GetValue();
  }

  //! Sets shape.
  //! \param[in] shape shape to set.
  void SetShape(const TopoDS_Shape& shape)
  {
    ActParamTool::AsShape( this->Parameter(PID_Shape) )->SetShape(shape);
  }

  //! \return shape.
  TopoDS_Shape GetShape() const
  {
    return ActParamTool::AsShape( this->Parameter(PID_Shape) )->GetShape();
  }

protected:

  exeCSG_DrillHoleNode();

};

#endif
