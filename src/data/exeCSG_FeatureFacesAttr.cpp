//-----------------------------------------------------------------------------
// Created on: 29 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_FeatureFacesAttr.h>

// asiData includes
#include <asiData.h>

// OCCT includes
#include <Standard_GUID.hxx>

//-----------------------------------------------------------------------------
// Construction & settling-down routines
//-----------------------------------------------------------------------------

//! Default constructor.
exeCSG_FeatureFacesAttr::exeCSG_FeatureFacesAttr() : TDF_Attribute()
{}

//! Settles down new Attribute to the given OCAF Label.
//! \param[in] Label TDF Label to settle down the new Attribute to.
//! \return newly created Attribute settled down onto the target Label.
Handle(exeCSG_FeatureFacesAttr) exeCSG_FeatureFacesAttr::Set(const TDF_Label& Label)
{
  Handle(exeCSG_FeatureFacesAttr) A;
  //
  if ( !Label.FindAttribute(GUID(), A) )
  {
    A = new exeCSG_FeatureFacesAttr();
    Label.AddAttribute(A);
  }
  return A;
}

//-----------------------------------------------------------------------------
// Accessors for Attribute's GUID
//-----------------------------------------------------------------------------

//! Returns statically defined GUID for the Attribute.
//! \return statically defined GUID.
const Standard_GUID& exeCSG_FeatureFacesAttr::GUID()
{
  static Standard_GUID AttrGUID("FDFB1CBD-FFE1-4C7D-8AC2-B98C3FA05F3A");
  return AttrGUID;
}

//! Accessor for GUID associated with this kind of OCAF Attribute.
//! \return GUID of the OCAF Attribute.
const Standard_GUID& exeCSG_FeatureFacesAttr::ID() const
{
  return GUID();
}

//-----------------------------------------------------------------------------
// Attribute's kernel methods:
//-----------------------------------------------------------------------------

//! Creates new instance of the Attribute which is not initially populated
//! with any data structures.
//! \return new instance of the Attribute.
Handle(TDF_Attribute) exeCSG_FeatureFacesAttr::NewEmpty() const
{
  return new exeCSG_FeatureFacesAttr();
}

//! Performs data transferring from the given OCAF Attribute to this one.
//! This method is mainly used by OCAF Undo/Redo kernel as a part of
//! backup functionality.
//! \param[in] mainAttr OCAF Attribute to copy data from.
void exeCSG_FeatureFacesAttr::Restore(const Handle(TDF_Attribute)& asiData_NotUsed(mainAttr))
{
  // Nothing is here
}

//! Supporting method for Copy/Paste functionality. Performs full copying of
//! the underlying data.
//! \param[in] into       where to paste.
//! \param[in] relocTable relocation table.
void exeCSG_FeatureFacesAttr::Paste(const Handle(TDF_Attribute)&       asiData_NotUsed(into),
                                    const Handle(TDF_RelocationTable)& asiData_NotUsed(relocTable)) const
{
  // Nothing is here
}

//-----------------------------------------------------------------------------
// Accessors for domain-specific data
//-----------------------------------------------------------------------------

//! Sets data to store.
//! \param[in] featureFaces data to store.
void exeCSG_FeatureFacesAttr::SetData(const Handle(exeCSG_FeatureFaces)& featureFaces)
{
  m_featureFaces = featureFaces;
}

//! Returns the stored data.
//! \return stored feature faces.
const Handle(exeCSG_FeatureFaces)& exeCSG_FeatureFacesAttr::GetData() const
{
  return m_featureFaces;
}
