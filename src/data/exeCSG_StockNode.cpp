//-----------------------------------------------------------------------------
// Created on: 22 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_StockNode.h>

//-----------------------------------------------------------------------------

Handle(ActAPI_INode) exeCSG_StockNode::Instance()
{
  return new exeCSG_StockNode();
}

//-----------------------------------------------------------------------------

//! Allocation is allowed only via Instance method.
exeCSG_StockNode::exeCSG_StockNode() : ActData_BaseNode()
{
  REGISTER_PARAMETER(Name,  PID_Name);
  REGISTER_PARAMETER(Real,  PID_LeftX);
  REGISTER_PARAMETER(Real,  PID_LeftY);
  REGISTER_PARAMETER(Real,  PID_LeftZ);
  REGISTER_PARAMETER(Real,  PID_RightX);
  REGISTER_PARAMETER(Real,  PID_RightY);
  REGISTER_PARAMETER(Real,  PID_RightZ);
  REGISTER_PARAMETER(Shape, PID_Shape);
  //
  REGISTER_PARAMETER(TreeFunction, PID_FuncSelfEval);
}

//-----------------------------------------------------------------------------

void exeCSG_StockNode::Init()
{
  // Set names for some Parameters (for user-friendly dump)
  this->InitParameter(PID_Name,         "Name");
  this->InitParameter(PID_LeftX,        "LeftX");
  this->InitParameter(PID_LeftY,        "LeftY");
  this->InitParameter(PID_LeftZ,        "LeftZ");
  this->InitParameter(PID_RightX,       "RightX");
  this->InitParameter(PID_RightY,       "RightY");
  this->InitParameter(PID_RightZ,       "RightZ");
  this->InitParameter(PID_Shape,        "Shape");
  this->InitParameter(PID_FuncSelfEval, "Self-evaluation");

  // Initialize stock properties
  this->SetLeftX(0.0);
  this->SetLeftY(0.0);
  this->SetLeftZ(0.0);
  this->SetRightX(0.0);
  this->SetRightY(0.0);
  this->SetRightZ(0.0);
  this->SetShape( TopoDS_Shape() );
}

//-----------------------------------------------------------------------------

TCollection_ExtendedString exeCSG_StockNode::GetName()
{
  return ActParamTool::AsName( this->Parameter(PID_Name) )->GetValue();
}

//-----------------------------------------------------------------------------

void exeCSG_StockNode::SetName(const TCollection_ExtendedString& name)
{
  ActParamTool::AsName( this->Parameter(PID_Name) )->SetValue(name);
}
