//-----------------------------------------------------------------------------
// Created on: 22 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_DrillHoleNode.h>

//-----------------------------------------------------------------------------

Handle(ActAPI_INode) exeCSG_DrillHoleNode::Instance()
{
  return new exeCSG_DrillHoleNode();
}

//-----------------------------------------------------------------------------

//! Allocation is allowed only via Instance method.
exeCSG_DrillHoleNode::exeCSG_DrillHoleNode() : exeCSG_FeatureNode()
{
  REGISTER_PARAMETER(Real,  PID_Radius);
  REGISTER_PARAMETER(Real,  PID_Depth);
  REGISTER_PARAMETER(Shape, PID_Shape);
  //
  REGISTER_PARAMETER(TreeFunction, PID_FuncSelfEval);
}

//-----------------------------------------------------------------------------

void exeCSG_DrillHoleNode::Init(const gp_Ax3& placement,
                                const double  radius,
                                const double  depth)
{
  exeCSG_FeatureNode::Init(placement);

  // Set names for some Parameters (for user-friendly dump)
  this->InitParameter(PID_Radius, "Radius");
  this->InitParameter(PID_Depth,  "Depth");

  // Initialize feature properties
  this->SetRadius(radius);
  this->SetDepth(depth);
  this->SetShape( TopoDS_Shape() );
}
