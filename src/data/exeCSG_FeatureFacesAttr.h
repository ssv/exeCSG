//-----------------------------------------------------------------------------
// Created on: 29 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_FeatureFacesAttr_h
#define exeCSG_FeatureFacesAttr_h

// exeCSG includes
#include <exeCSG_FeatureFaces.h>

// OCCT includes
#include <TDF_Attribute.hxx>
#include <TDF_Label.hxx>

//! OCAF Attribute representing feature faces.
class exeCSG_FeatureFacesAttr : public TDF_Attribute
{
public:

  // OCCT RTTI
  DEFINE_STANDARD_RTTI_INLINE(exeCSG_FeatureFacesAttr, TDF_Attribute)

// Construction & settling-down routines:
public:

  exeCSG_FeatureFacesAttr();

  static Handle(exeCSG_FeatureFacesAttr)
    Set(const TDF_Label& Label);

// GUID accessors:
public:

  static const Standard_GUID&
    GUID();

  virtual const Standard_GUID&
    ID() const;

// Attribute's kernel methods:
public:

  virtual Handle(TDF_Attribute)
    NewEmpty() const;

  virtual void
    Restore(const Handle(TDF_Attribute)& mainAttr);

  virtual void
    Paste(const Handle(TDF_Attribute)&       into,
          const Handle(TDF_RelocationTable)& relocTable) const;

// Accessors for domain-specific data:
public:

  void
    SetData(const Handle(exeCSG_FeatureFaces)& featureFaces);

  const Handle(exeCSG_FeatureFaces)&
    GetData() const;

// Members:
private:

  //! Stored feature faces.
  Handle(exeCSG_FeatureFaces) m_featureFaces;

};

#endif
