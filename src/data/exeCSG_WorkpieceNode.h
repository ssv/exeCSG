//-----------------------------------------------------------------------------
// Created on: 22 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_WorkpieceNode_h
#define exeCSG_WorkpieceNode_h

// exeCSG includes
#include <exeCSG_FeatureFacesParameter.h>

// Active Data includes
#include <ActData_BaseNode.h>
#include <ActData_ParameterFactory.h>

//! Workpiece designed by features.
class exeCSG_WorkpieceNode : public ActData_BaseNode
{
public:

  // OCCT RTTI
  DEFINE_STANDARD_RTTI_INLINE(exeCSG_WorkpieceNode, ActData_BaseNode)

  // Automatic registration of Node type in global factory
  DEFINE_NODE_FACTORY(exeCSG_WorkpieceNode, Instance)

public:

  //! IDs for the underlying Parameters.
  enum ParamId
  {
  //---------------------//
  // Common              //
  //---------------------//
    PID_Name,            //!< Name of the Node.
  //---------------------//
    PID_Shape,           //!< Boundary representation of workpiece.
  //---------------------//
    PID_FuncMillingEval, //!< Tree Function for feature milling.
  //---------------------//
    PID_FeatureFaces,    //!< Parameter to store feature faces.
  //---------------------//
    PID_Last = PID_Name + ActData_BaseNode::RESERVED_PARAM_RANGE
  };

public:

  static Handle(ActAPI_INode) Instance();

// Initialization:
public:

  void Init();

// Generic naming support:
public:

  virtual TCollection_ExtendedString
    GetName();

  virtual void
    SetName(const TCollection_ExtendedString& name);

// Convenience methods:
public:

  //! Sets shape.
  //! \param[in] shape shape to set.
  void SetShape(const TopoDS_Shape& shape)
  {
    ActParamTool::AsShape( this->Parameter(PID_Shape) )->SetShape(shape);
  }

  //! \return shape.
  TopoDS_Shape GetShape() const
  {
    return ActParamTool::AsShape( this->Parameter(PID_Shape) )->GetShape();
  }

  //! Sets feature faces.
  //! \param data [in] feature faces to set.
  void SetFeatureFaces(const Handle(exeCSG_FeatureFaces)& data)
  {
    Handle(exeCSG_FeatureFacesParameter)::DownCast( this->Parameter(PID_FeatureFaces) )->SetFeatureFaces(data);
  }

  //! \return stored feature faces.
  Handle(exeCSG_FeatureFaces) GetFeatureFaces() const
  {
    return Handle(exeCSG_FeatureFacesParameter)::DownCast( this->Parameter(PID_FeatureFaces) )->GetFeatureFaces();
  }

protected:

  exeCSG_WorkpieceNode();

};

#endif
