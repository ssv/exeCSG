//-----------------------------------------------------------------------------
// Created on: 29 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_FeatureFacesParameter_h
#define exeCSG_FeatureFacesParameter_h

// exeCSG includes
#include <exeCSG_FeatureFacesAttr.h>

// Active Data includes
#include <ActData_UserParameter.h>
#include <ActData_Common.h>
#include <ActData_ParameterDTO.h>

//-----------------------------------------------------------------------------
// Parameter DTO
//-----------------------------------------------------------------------------

DEFINE_STANDARD_HANDLE(exeCSG_FeatureFacesDTO, ActData_ParameterDTO)

//! Data Transfer Object (DTO) corresponding to data wrapped with
//! Feature Faces Parameter without any OCAF connectivity.
class exeCSG_FeatureFacesDTO : public ActData_ParameterDTO
{
public:

  // OCCT RTTI
  DEFINE_STANDARD_RTTI_INLINE(exeCSG_FeatureFacesDTO, ActData_ParameterDTO)

public:

  //! Constructor accepting GID.
  //! \param GID [in] GID.
  exeCSG_FeatureFacesDTO(const ActAPI_ParameterGID& GID) : ActData_ParameterDTO(GID, Parameter_UNDEFINED) {}

public:

  Handle(exeCSG_FeatureFaces) FeatureFaces; //!< Feature Faces.

};

//-----------------------------------------------------------------------------
// Parameter
//-----------------------------------------------------------------------------

DEFINE_STANDARD_HANDLE(exeCSG_FeatureFacesParameter, ActData_UserParameter)

//! Node Parameter representing Feature Faces.
class exeCSG_FeatureFacesParameter : public ActData_UserParameter
{
public:

  // OCCT RTTI
  DEFINE_STANDARD_RTTI_INLINE(exeCSG_FeatureFacesParameter, ActData_UserParameter)

public:

  static Handle(exeCSG_FeatureFacesParameter)
    Instance();

public:

  void
    SetFeatureFaces(const Handle(exeCSG_FeatureFaces)& featureFaces,
                    const ActAPI_ModificationType      MType           = MT_Touched,
                    const bool                         doResetValidity = true,
                    const bool                         doResetPending  = true);

  Handle(exeCSG_FeatureFaces)
    GetFeatureFaces();

protected:

  exeCSG_FeatureFacesParameter();

private:

  virtual bool isWellFormed() const;
  virtual int parameterType() const;

private:

  virtual void
    setFromDTO(const Handle(ActData_ParameterDTO)& DTO,
               const ActAPI_ModificationType       MType = MT_Touched,
               const bool                          doResetValidity = true,
               const bool                          doResetPending = true);

  virtual Handle(ActData_ParameterDTO)
    createDTO(const ActAPI_ParameterGID& GID);

protected:

  //! Tags for the underlying CAF Labels.
  enum Datum
  {
    DS_FeatureFaces = ActData_UserParameter::DS_DatumLast,
    DS_DatumLast = DS_FeatureFaces + RESERVED_DATUM_RANGE
  };

};

#endif
