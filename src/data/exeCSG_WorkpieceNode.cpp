//-----------------------------------------------------------------------------
// Created on: 22 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_WorkpieceNode.h>

//-----------------------------------------------------------------------------

Handle(ActAPI_INode) exeCSG_WorkpieceNode::Instance()
{
  return new exeCSG_WorkpieceNode();
}

//-----------------------------------------------------------------------------

//! Allocation is allowed only via Instance method.
exeCSG_WorkpieceNode::exeCSG_WorkpieceNode() : ActData_BaseNode()
{
  REGISTER_PARAMETER(Name,  PID_Name);
  REGISTER_PARAMETER(Shape, PID_Shape);
  //
  REGISTER_PARAMETER(TreeFunction, PID_FuncMillingEval);

  // Register custom CSG Parameters
  this->registerParameter(PID_FeatureFaces, exeCSG_FeatureFacesParameter::Instance(), false);
}

//-----------------------------------------------------------------------------

void exeCSG_WorkpieceNode::Init()
{
  // Set names for some Parameters (for user-friendly dump)
  this->InitParameter(PID_Name,         "Name");
  this->InitParameter(PID_Shape,        "Shape");
  this->InitParameter(PID_FeatureFaces, "Feature faces");

  // Initialize shape
  this->SetShape( TopoDS_Shape() );

  // Initialize feature faces
  this->SetFeatureFaces(new exeCSG_FeatureFaces);
}

//-----------------------------------------------------------------------------

TCollection_ExtendedString exeCSG_WorkpieceNode::GetName()
{
  return ActParamTool::AsName( this->Parameter(PID_Name) )->GetValue();
}

//-----------------------------------------------------------------------------

void exeCSG_WorkpieceNode::SetName(const TCollection_ExtendedString& name)
{
  ActParamTool::AsName( this->Parameter(PID_Name) )->SetValue(name);
}
