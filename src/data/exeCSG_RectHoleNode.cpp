//-----------------------------------------------------------------------------
// Created on: 22 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_RectHoleNode.h>

//-----------------------------------------------------------------------------

Handle(ActAPI_INode) exeCSG_RectHoleNode::Instance()
{
  return new exeCSG_RectHoleNode();
}

//-----------------------------------------------------------------------------

//! Allocation is allowed only via Instance method.
exeCSG_RectHoleNode::exeCSG_RectHoleNode() : exeCSG_FeatureNode()
{
  REGISTER_PARAMETER(Real,  PID_Width);
  REGISTER_PARAMETER(Real,  PID_Height);
  REGISTER_PARAMETER(Real,  PID_Depth);
  REGISTER_PARAMETER(Shape, PID_Shape);
  //
  REGISTER_PARAMETER(TreeFunction, PID_FuncSelfEval);
}

//-----------------------------------------------------------------------------

void exeCSG_RectHoleNode::Init(const gp_Ax3& placement,
                               const double  width,
                               const double  height,
                               const double  depth)
{
  exeCSG_FeatureNode::Init(placement);

  // Set names for some Parameters (for user-friendly dump)
  this->InitParameter(PID_Width,  "Width");
  this->InitParameter(PID_Height, "Height");
  this->InitParameter(PID_Depth,  "Depth");

  // Initialize feature properties
  this->SetWidth(width);
  this->SetHeight(height);
  this->SetDepth(depth);
  this->SetShape( TopoDS_Shape() );
}
