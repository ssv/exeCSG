//-----------------------------------------------------------------------------
// Created on: 22 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_RectHoleNode_h
#define exeCSG_RectHoleNode_h

// exeCSG includes
#include <exeCSG_FeatureNode.h>

//! Rectangular hole feature.
class exeCSG_RectHoleNode : public exeCSG_FeatureNode
{
public:

  // OCCT RTTI
  DEFINE_STANDARD_RTTI_INLINE(exeCSG_RectHoleNode, exeCSG_FeatureNode)

  // Automatic registration of Node type in global factory
  DEFINE_NODE_FACTORY(exeCSG_RectHoleNode, Instance)

public:

  //! IDs for the underlying Parameters.
  enum ParamId
  {
    PID_Width = PID_FeatureNodeLast,
    PID_Height,
    PID_Depth,
  //-------------------//
    PID_Shape,         //!< Evaluated feature shape.
  //-------------------//
    PID_FuncSelfEval,  //!< Self-evaluation Tree Function.
  //-------------------//
    PID_RectHoleLast
  };

public:

  static Handle(ActAPI_INode) Instance();

// Initialization:
public:

  void Init(const gp_Ax3& placement,
            const double  width,
            const double  height,
            const double  depth);

// Convenience methods:
public:

  //! Sets width value.
  //! \param[in] val value to set.
  void SetWidth(const double val)
  {
    ActParamTool::AsReal( this->Parameter(PID_Width) )->SetValue(val);
  }

  //! \return width value.
  double GetWidth() const
  {
    return ActParamTool::AsReal( this->Parameter(PID_Width) )->GetValue();
  }

  //! Sets height value.
  //! \param[in] val value to set.
  void SetHeight(const double val)
  {
    ActParamTool::AsReal( this->Parameter(PID_Height) )->SetValue(val);
  }

  //! \return height value.
  double GetHeight() const
  {
    return ActParamTool::AsReal( this->Parameter(PID_Height) )->GetValue();
  }

  //! Sets depth value.
  //! \param[in] val value to set.
  void SetDepth(const double val)
  {
    ActParamTool::AsReal( this->Parameter(PID_Depth) )->SetValue(val);
  }

  //! \return depth value.
  double GetDepth() const
  {
    return ActParamTool::AsReal( this->Parameter(PID_Depth) )->GetValue();
  }

  //! Sets shape.
  //! \param[in] shape shape to set.
  void SetShape(const TopoDS_Shape& shape)
  {
    ActParamTool::AsShape( this->Parameter(PID_Shape) )->SetShape(shape);
  }

  //! \return shape.
  TopoDS_Shape GetShape() const
  {
    return ActParamTool::AsShape( this->Parameter(PID_Shape) )->GetShape();
  }

protected:

  exeCSG_RectHoleNode();

};

#endif
