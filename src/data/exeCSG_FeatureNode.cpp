//-----------------------------------------------------------------------------
// Created on: 22 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_FeatureNode.h>

//-----------------------------------------------------------------------------

Handle(ActAPI_INode) exeCSG_FeatureNode::Instance()
{
  return new exeCSG_FeatureNode();
}

//-----------------------------------------------------------------------------

//! Allocation is allowed only via Instance method.
exeCSG_FeatureNode::exeCSG_FeatureNode() : ActData_BaseNode()
{
  REGISTER_PARAMETER(Name, PID_Name);
  REGISTER_PARAMETER(Real, PID_OX);
  REGISTER_PARAMETER(Real, PID_OY);
  REGISTER_PARAMETER(Real, PID_OZ);
  REGISTER_PARAMETER(Real, PID_ZetaX);
  REGISTER_PARAMETER(Real, PID_ZetaY);
  REGISTER_PARAMETER(Real, PID_ZetaZ);
  REGISTER_PARAMETER(Real, PID_KsiX);
  REGISTER_PARAMETER(Real, PID_KsiY);
  REGISTER_PARAMETER(Real, PID_KsiZ);
}

//-----------------------------------------------------------------------------

void exeCSG_FeatureNode::Init(const gp_Ax3& placement)
{
  // Set names for some Parameters (for user-friendly dump)
  this->InitParameter(PID_Name,  "Name");
  this->InitParameter(PID_OX,    "OX");
  this->InitParameter(PID_OY,    "OY");
  this->InitParameter(PID_OZ,    "OZ");
  this->InitParameter(PID_ZetaX, "ZetaX");
  this->InitParameter(PID_ZetaY, "ZetaY");
  this->InitParameter(PID_ZetaZ, "ZetaZ");
  this->InitParameter(PID_KsiX,  "KsiX");
  this->InitParameter(PID_KsiY,  "KsiY");
  this->InitParameter(PID_KsiZ,  "KsiZ");

  // Initialize placement parameters
  this->SetOX    ( placement.Location().X() );
  this->SetOY    ( placement.Location().Y() );
  this->SetOZ    ( placement.Location().Z() );
  this->SetZetaX ( placement.Direction().X() );
  this->SetZetaY ( placement.Direction().Y() );
  this->SetZetaZ ( placement.Direction().Z() );
  this->SetKsiX  ( placement.XDirection().X() );
  this->SetKsiY  ( placement.XDirection().Y() );
  this->SetKsiZ  ( placement.XDirection().Z() );
}

//-----------------------------------------------------------------------------

TCollection_ExtendedString exeCSG_FeatureNode::GetName()
{
  return ActParamTool::AsName( this->Parameter(PID_Name) )->GetValue();
}

//-----------------------------------------------------------------------------

void exeCSG_FeatureNode::SetName(const TCollection_ExtendedString& name)
{
  ActParamTool::AsName( this->Parameter(PID_Name) )->SetValue(name);
}
