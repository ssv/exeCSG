//-----------------------------------------------------------------------------
// Created on: 29 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_FeatureFacesParameter.h>

// Active Data includes
#include <ActData_Utils.h>

//-----------------------------------------------------------------------------
// Parameter
//-----------------------------------------------------------------------------

//! Default constructor.
exeCSG_FeatureFacesParameter::exeCSG_FeatureFacesParameter() : ActData_UserParameter()
{}

//! Ensures correct construction of the Parameter object, e.g. prevents
//! allocating it in stack memory.
//! \return Parameter instance.
Handle(exeCSG_FeatureFacesParameter) exeCSG_FeatureFacesParameter::Instance()
{
  return new exeCSG_FeatureFacesParameter();
}

//! Sets feature faces.
//! \param featureFaces    [in] feature faces to set.
//! \param MType           [in] modification type.
//! \param doResetValidity [in] indicates whether to reset validity flag.
//! \param doResetPending  [in] indicates whether this Parameter must lose its
//!                             PENDING (or out-dated) property.
void exeCSG_FeatureFacesParameter::SetFeatureFaces(const Handle(exeCSG_FeatureFaces)& featureFaces,
                                                   const ActAPI_ModificationType      MType,
                                                   const bool                         doResetValidity,
                                                   const bool                         doResetPending)
{
  if ( this->IsDetached() )
    Standard_ProgramError::Raise("Cannot access detached data");

  // Settle down an attribute an populate it with data
  TDF_Label                       dataLab = ActData_Utils::ChooseLabelByTag(m_label, DS_FeatureFaces, true);
  Handle(exeCSG_FeatureFacesAttr) attr    = exeCSG_FeatureFacesAttr::Set(dataLab);
  //
  attr->SetData(featureFaces);

  // Mark root label of the Parameter as modified (Touched, Impacted or Silent)
  SPRING_INTO_FUNCTION(MType)
  // Reset Parameter's validity flag if requested
  RESET_VALIDITY(doResetValidity)
  // Reset Parameter's PENDING property
  RESET_PENDING(doResetPending)
}

//! Accessor for the stored feature faces.
//! \return stored feature faces.
Handle(exeCSG_FeatureFaces) exeCSG_FeatureFacesParameter::GetFeatureFaces()
{
  if ( !this->IsWellFormed() )
    Standard_ProgramError::Raise("Data inconsistent");

  // Choose a data label ensuring not to create it
  TDF_Label dataLab = ActData_Utils::ChooseLabelByTag(m_label, DS_FeatureFaces, false);

  // Get feature faces attribute
  Handle(exeCSG_FeatureFacesAttr) attr;
  dataLab.FindAttribute(exeCSG_FeatureFacesAttr::GUID(), attr);
  //
  if ( attr.IsNull() )
    return NULL;

  return attr->GetData();
}

//! Checks if this Parameter object is mapped onto CAF data structure in a
//! correct way.
//! \return true if the object is well-formed, false -- otherwise.
bool exeCSG_FeatureFacesParameter::isWellFormed() const
{
  if ( !ActData_Utils::CheckLabelAttr( m_label, DS_FeatureFaces,
                                       exeCSG_FeatureFacesAttr::GUID() ) )
    return false;

  return true;
}

//! Returns Parameter type.
//! \return Parameter type.
int exeCSG_FeatureFacesParameter::parameterType() const
{
  return Parameter_FeatureFaces;
}

//-----------------------------------------------------------------------------
// DTO construction
//-----------------------------------------------------------------------------

//! Populates Parameter from the passed DTO.
//! \param DTO             [in] DTO to source data from.
//! \param MType           [in] modification type.
//! \param doResetValidity [in] indicates whether validity flag must be
//!                             reset or not.
//! \param doResetPending  [in] indicates whether pending flag must be reset
//!                             or not.
void exeCSG_FeatureFacesParameter::setFromDTO(const Handle(ActData_ParameterDTO)& DTO,
                                              const ActAPI_ModificationType       MType,
                                              const bool                          doResetValidity,
                                              const bool                          doResetPending)
{
  Handle(exeCSG_FeatureFacesDTO) MyDTO = Handle(exeCSG_FeatureFacesDTO)::DownCast(DTO);
  this->SetFeatureFaces(MyDTO->FeatureFaces, MType, doResetValidity, doResetPending);
}

//! Creates and populates DTO.
//! \param GID [in] ready-to-use GID for DTO.
//! \return constructed DTO instance.
Handle(ActData_ParameterDTO)
  exeCSG_FeatureFacesParameter::createDTO(const ActAPI_ParameterGID& GID)
{
  Handle(exeCSG_FeatureFacesDTO) res = new exeCSG_FeatureFacesDTO(GID);
  res->FeatureFaces = this->GetFeatureFaces();
  return res;
}
