//-----------------------------------------------------------------------------
// Created on: 22 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_StockNode_h
#define exeCSG_StockNode_h

// Active Data includes
#include <ActData_BaseNode.h>
#include <ActData_ParameterFactory.h>

//! Stock model.
class exeCSG_StockNode : public ActData_BaseNode
{
public:

  // OCCT RTTI
  DEFINE_STANDARD_RTTI_INLINE(exeCSG_StockNode, ActData_BaseNode)

  // Automatic registration of Node type in global factory
  DEFINE_NODE_FACTORY(exeCSG_StockNode, Instance)

public:

  //! IDs for the underlying Parameters.
  enum ParamId
  {
  //-------------------//
  // Common            //
  //-------------------//
    PID_Name,          //!< Name of the Node.
  //-------------------//
    PID_LeftX,         //!< X coordinate of the left corner.
    PID_LeftY,         //!< Y coordinate of the left corner.
    PID_LeftZ,         //!< Z coordinate of the left corner.
    PID_RightX,        //!< X coordinate of the right corner.
    PID_RightY,        //!< Y coordinate of the right corner.
    PID_RightZ,        //!< Z coordinate of the right corner.
  //-------------------//
    PID_Shape,         //!< Evaluated stock shape.
  //-------------------//
    PID_FuncSelfEval,  //!< Self-evaluation Tree Function.
  //-------------------//
    PID_Last = PID_Name + ActData_BaseNode::RESERVED_PARAM_RANGE
  };

public:

  static Handle(ActAPI_INode) Instance();

// Initialization:
public:

  void Init();

// Generic naming support:
public:

  virtual TCollection_ExtendedString
    GetName();

  virtual void
    SetName(const TCollection_ExtendedString& name);

// Convenience methods:
public:

  //! Sets LeftX value.
  //! \param[in] val value to set.
  void SetLeftX(const double val)
  {
    ActParamTool::AsReal( this->Parameter(PID_LeftX) )->SetValue(val);
  }

  //! \return LeftX value.
  double GetLeftX() const
  {
    return ActParamTool::AsReal( this->Parameter(PID_LeftX) )->GetValue();
  }

  //! Sets LeftY value.
  //! \param[in] val value to set.
  void SetLeftY(const double val)
  {
    ActParamTool::AsReal( this->Parameter(PID_LeftY) )->SetValue(val);
  }

  //! \return LeftY value.
  double GetLeftY() const
  {
    return ActParamTool::AsReal( this->Parameter(PID_LeftY) )->GetValue();
  }

  //! Sets LeftZ value.
  //! \param[in] val value to set.
  void SetLeftZ(const double val)
  {
    ActParamTool::AsReal( this->Parameter(PID_LeftZ) )->SetValue(val);
  }

  //! \return LeftZ value.
  double GetLeftZ() const
  {
    return ActParamTool::AsReal( this->Parameter(PID_LeftZ) )->GetValue();
  }

  //! Sets RightX value.
  //! \param[in] val value to set.
  void SetRightX(const double val)
  {
    ActParamTool::AsReal( this->Parameter(PID_RightX) )->SetValue(val);
  }

  //! \return RightX value.
  double GetRightX() const
  {
    return ActParamTool::AsReal( this->Parameter(PID_RightX) )->GetValue();
  }

  //! Sets RightY value.
  //! \param[in] val value to set.
  void SetRightY(const double val)
  {
    ActParamTool::AsReal( this->Parameter(PID_RightY) )->SetValue(val);
  }

  //! \return RightY value.
  double GetRightY() const
  {
    return ActParamTool::AsReal( this->Parameter(PID_RightY) )->GetValue();
  }

  //! Sets RightZ value.
  //! \param[in] val value to set.
  void SetRightZ(const double val)
  {
    ActParamTool::AsReal( this->Parameter(PID_RightZ) )->SetValue(val);
  }

  //! \return RightZ value.
  double GetRightZ() const
  {
    return ActParamTool::AsReal( this->Parameter(PID_RightZ) )->GetValue();
  }

  //! Sets shape.
  //! \param[in] shape shape to set.
  void SetShape(const TopoDS_Shape& shape)
  {
    ActParamTool::AsShape( this->Parameter(PID_Shape) )->SetShape(shape);
  }

  //! \return shape.
  TopoDS_Shape GetShape() const
  {
    return ActParamTool::AsShape( this->Parameter(PID_Shape) )->GetShape();
  }

protected:

  exeCSG_StockNode();

};

#endif
