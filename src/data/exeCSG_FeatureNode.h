//-----------------------------------------------------------------------------
// Created on: 22 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_FeatureNode_h
#define exeCSG_FeatureNode_h

// Active Data includes
#include <ActData_BaseNode.h>
#include <ActData_ParameterFactory.h>

// OCCT includes
#include <gp_Ax3.hxx>

//! Base class for features.
class exeCSG_FeatureNode : public ActData_BaseNode
{
public:

  // OCCT RTTI
  DEFINE_STANDARD_RTTI_INLINE(exeCSG_FeatureNode, ActData_BaseNode)

  // Automatic registration of Node type in global factory
  DEFINE_NODE_FACTORY(exeCSG_FeatureNode, Instance)

public:

  //! IDs for the underlying Parameters.
  enum ParamId
  {
  //-------------------//
  // Common            //
  //-------------------//
    PID_Name,          //!< Name of the Node.
  //-------------------//
    PID_OX,            //!< Origin (X).
    PID_OY,            //!< Origin (Y).
    PID_OZ,            //!< Origin (Z).
    PID_ZetaX,         //!< Normal (X).
    PID_ZetaY,         //!< Normal (Y).
    PID_ZetaZ,         //!< Normal (Z).
    PID_KsiX,          //!< Ksi direction (X).
    PID_KsiY,          //!< Ksi direction (Y).
    PID_KsiZ,          //!< Ksi direction (Z).
  //-------------------//
    PID_FeatureNodeLast
  };

public:

  static Handle(ActAPI_INode) Instance();

// Initialization:
public:

  void Init(const gp_Ax3& placement);

// Generic naming support:
public:

  virtual TCollection_ExtendedString
    GetName();

  virtual void
    SetName(const TCollection_ExtendedString& name);

// Convenience methods:
public:

  //! Sets OX value.
  //! \param[in] val value to set.
  void SetOX(const double val)
  {
    ActParamTool::AsReal( this->Parameter(PID_OX) )->SetValue(val);
  }

  //! \return OX value.
  double GetOX() const
  {
    return ActParamTool::AsReal( this->Parameter(PID_OX) )->GetValue();
  }

  //! Sets OY value.
  //! \param[in] val value to set.
  void SetOY(const double val)
  {
    ActParamTool::AsReal( this->Parameter(PID_OY) )->SetValue(val);
  }

  //! \return OY value.
  double GetOY() const
  {
    return ActParamTool::AsReal( this->Parameter(PID_OY) )->GetValue();
  }

  //! Sets OZ value.
  //! \param[in] val value to set.
  void SetOZ(const double val)
  {
    ActParamTool::AsReal( this->Parameter(PID_OZ) )->SetValue(val);
  }

  //! \return OZ value.
  double GetOZ() const
  {
    return ActParamTool::AsReal( this->Parameter(PID_OZ) )->GetValue();
  }

  //! Sets ZetaX value.
  //! \param[in] val value to set.
  void SetZetaX(const double val)
  {
    ActParamTool::AsReal( this->Parameter(PID_ZetaX) )->SetValue(val);
  }

  //! \return ZetaX value.
  double GetZetaX() const
  {
    return ActParamTool::AsReal( this->Parameter(PID_ZetaX) )->GetValue();
  }

  //! Sets ZetaY value.
  //! \param[in] val value to set.
  void SetZetaY(const double val)
  {
    ActParamTool::AsReal( this->Parameter(PID_ZetaY) )->SetValue(val);
  }

  //! \return ZetaY value.
  double GetZetaY() const
  {
    return ActParamTool::AsReal( this->Parameter(PID_ZetaY) )->GetValue();
  }

  //! Sets ZetaZ value.
  //! \param[in] val value to set.
  void SetZetaZ(const double val)
  {
    ActParamTool::AsReal( this->Parameter(PID_ZetaZ) )->SetValue(val);
  }

  //! \return ZetaZ value.
  double GetZetaZ() const
  {
    return ActParamTool::AsReal( this->Parameter(PID_ZetaZ) )->GetValue();
  }

  //! Sets KsiX value.
  //! \param[in] val value to set.
  void SetKsiX(const double val)
  {
    ActParamTool::AsReal( this->Parameter(PID_KsiX) )->SetValue(val);
  }

  //! \return KsiX value.
  double GetKsiX() const
  {
    return ActParamTool::AsReal( this->Parameter(PID_KsiX) )->GetValue();
  }

  //! Sets KsiY value.
  //! \param[in] val value to set.
  void SetKsiY(const double val)
  {
    ActParamTool::AsReal( this->Parameter(PID_KsiY) )->SetValue(val);
  }

  //! \return KsiY value.
  double GetKsiY() const
  {
    return ActParamTool::AsReal( this->Parameter(PID_KsiY) )->GetValue();
  }

  //! Sets KsiZ value.
  //! \param[in] val value to set.
  void SetKsiZ(const double val)
  {
    ActParamTool::AsReal( this->Parameter(PID_KsiZ) )->SetValue(val);
  }

  //! \return KsiZ value.
  double GetKsiZ() const
  {
    return ActParamTool::AsReal( this->Parameter(PID_KsiZ) )->GetValue();
  }

protected:

  exeCSG_FeatureNode();

};

#endif
