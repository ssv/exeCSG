//-----------------------------------------------------------------------------
// Created on: 26 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_StockDataProvider_h
#define exeCSG_StockDataProvider_h

// exeCSG includes
#include <exeCSG_StockNode.h>

// asiVisu includes
#include <asiVisu_ShapeDataProvider.h>

//! Data provider for a shape representing a stock model.
class exeCSG_StockDataProvider : public asiVisu_ShapeDataProvider
{
public:

  // OCCT RTTI
  DEFINE_STANDARD_RTTI_INLINE(exeCSG_StockDataProvider, asiVisu_ShapeDataProvider)

public:

  exeCSG_StockDataProvider(const Handle(exeCSG_StockNode)& N);

public:

  virtual TopoDS_Shape
    GetShape() const;

};

#endif
