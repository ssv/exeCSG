//-----------------------------------------------------------------------------
// Created on: 27 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_WorkpieceDataProvider_h
#define exeCSG_WorkpieceDataProvider_h

// exeCSG includes
#include <exeCSG_WorkpieceNode.h>

// asiVisu includes
#include <asiVisu_ShapeDataProvider.h>

//! Data provider for a shape representing a workpiece model.
class exeCSG_WorkpieceDataProvider : public asiVisu_ShapeDataProvider
{
public:

  // OCCT RTTI
  DEFINE_STANDARD_RTTI_INLINE(exeCSG_WorkpieceDataProvider, asiVisu_ShapeDataProvider)

public:

  exeCSG_WorkpieceDataProvider(const Handle(exeCSG_WorkpieceNode)& N);

public:

  virtual TopoDS_Shape
    GetShape() const;

};

#endif
