//-----------------------------------------------------------------------------
// Created on: 27 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_WorkpieceDataProvider.h>

//-----------------------------------------------------------------------------

//! Creates data provider for the given Node.
//! \param N [in] source Node.
exeCSG_WorkpieceDataProvider::exeCSG_WorkpieceDataProvider(const Handle(exeCSG_WorkpieceNode)& N)
: asiVisu_ShapeDataProvider(N->GetId(), NULL) // List of Parameters is gathered just after
{
  m_params = ( ActParamStream() << N->Parameter(exeCSG_WorkpieceNode::PID_Shape) ).List;
}

//-----------------------------------------------------------------------------

//! \return shape to visualize.
TopoDS_Shape exeCSG_WorkpieceDataProvider::GetShape() const
{
  return ActParamTool::AsShape( m_params->Value(1) )->GetShape();
}
