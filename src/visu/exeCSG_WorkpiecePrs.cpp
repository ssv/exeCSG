//-----------------------------------------------------------------------------
// Created on: 27 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_WorkpiecePrs.h>

// exeCSG includes
#include <exeCSG_WorkpieceDataProvider.h>

// asiVisu includes
#include <asiVisu_ShapePipeline.h>

// VTK includes
#include <vtkMapper.h>
#include <vtkProperty.h>

//-----------------------------------------------------------------------------

//! Creates a Presentation object.
//! \param N [in] Data Node to create a Presentation for.
exeCSG_WorkpiecePrs::exeCSG_WorkpiecePrs(const Handle(ActAPI_INode)& N)
: asiVisu_Prs(N)
{
  // Create pipeline for phase cylinder
  Handle(asiVisu_ShapePipeline) pl = new asiVisu_ShapePipeline;
  //
  Handle(exeCSG_WorkpieceDataProvider)
    dp = new exeCSG_WorkpieceDataProvider( Handle(exeCSG_WorkpieceNode)::DownCast(N) );
  //
  this->addPipeline       (Pipeline_Main, pl);
  this->assignDataProvider(Pipeline_Main, dp);
  //
  pl->GetDisplayModeFilter()->SetDisplayMode(ShapeDisplayMode_Wireframe);
}

//-----------------------------------------------------------------------------

//! Factory method for Presentation.
//! \param N [in] Node to create a Presentation for.
//! \return new Presentation instance.
Handle(asiVisu_Prs) exeCSG_WorkpiecePrs::Instance(const Handle(ActAPI_INode)& N)
{
  return new exeCSG_WorkpiecePrs(N);
}

//-----------------------------------------------------------------------------

//! Returns true if the Presentation is visible, false -- otherwise.
//! \return true/false.
bool exeCSG_WorkpiecePrs::IsVisible() const
{
  return true;
}

//-----------------------------------------------------------------------------

//! Callback for initialization of Presentation pipelines.
void exeCSG_WorkpiecePrs::beforeInitPipelines()
{
  // Do nothing...
}

//-----------------------------------------------------------------------------

//! Callback for initialization of Presentation pipelines.
void exeCSG_WorkpiecePrs::afterInitPipelines()
{
  // Do nothing...
}

//-----------------------------------------------------------------------------

//! Callback for updating of Presentation pipelines invoked before the
//! kernel update routine starts.
void exeCSG_WorkpiecePrs::beforeUpdatePipelines() const
{
  // Do nothing...
}

//-----------------------------------------------------------------------------

//! Callback for updating of Presentation pipelines invoked after the
//! kernel update routine completes.
void exeCSG_WorkpiecePrs::afterUpdatePipelines() const
{
  // Do nothing...
}

//-----------------------------------------------------------------------------

//! Callback for highlighting.
//! \param theRenderer  [in] renderer.
//! \param thePickRes   [in] picking results.
//! \param theSelNature [in] selection nature (picking or detecting).
void exeCSG_WorkpiecePrs::highlight(vtkRenderer*                  theRenderer,
                                    const asiVisu_PickResult&     thePickRes,
                                    const asiVisu_SelectionNature theSelNature) const
{
  asiVisu_NotUsed(theRenderer);
  asiVisu_NotUsed(thePickRes);
  asiVisu_NotUsed(theSelNature);
}

//-----------------------------------------------------------------------------

//! Callback for highlighting reset.
//! \param theRenderer [in] renderer.
void exeCSG_WorkpiecePrs::unHighlight(vtkRenderer*                  theRenderer,
                                      const asiVisu_SelectionNature theSelNature) const
{
  asiVisu_NotUsed(theRenderer);
  asiVisu_NotUsed(theSelNature);
}

//-----------------------------------------------------------------------------

//! Callback for rendering.
//! \param theRenderer [in] renderer.
void exeCSG_WorkpiecePrs::renderPipelines(vtkRenderer* theRenderer) const
{
  asiVisu_NotUsed(theRenderer);
}

//-----------------------------------------------------------------------------

//! Callback for de-rendering.
//! \param theRenderer [in] renderer.
void exeCSG_WorkpiecePrs::deRenderPipelines(vtkRenderer* theRenderer) const
{
  asiVisu_NotUsed(theRenderer);
}
