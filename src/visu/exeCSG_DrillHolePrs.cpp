//-----------------------------------------------------------------------------
// Created on: 27 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_DrillHolePrs.h>

// exeCSG includes
#include <exeCSG_DrillHoleDataProvider.h>

// asiVisu includes
#include <asiVisu_ShapePipeline.h>

// VTK includes
#include <vtkMapper.h>
#include <vtkProperty.h>

//-----------------------------------------------------------------------------

//! Creates a Presentation object.
//! \param N [in] Data Node to create a Presentation for.
exeCSG_DrillHolePrs::exeCSG_DrillHolePrs(const Handle(ActAPI_INode)& N)
: asiVisu_Prs(N)
{
  // Create pipeline for phase cylinder
  Handle(asiVisu_ShapePipeline) pl = new asiVisu_ShapePipeline;
  //
  Handle(exeCSG_DrillHoleDataProvider)
    dp = new exeCSG_DrillHoleDataProvider( Handle(exeCSG_DrillHoleNode)::DownCast(N) );
  //
  pl->GetDisplayModeFilter()->SetDisplayMode(ShapeDisplayMode_Wireframe);
  //
  this->addPipeline       (Pipeline_Main, pl);
  this->assignDataProvider(Pipeline_Main, dp);
}

//-----------------------------------------------------------------------------

//! Factory method for Presentation.
//! \param N [in] Node to create a Presentation for.
//! \return new Presentation instance.
Handle(asiVisu_Prs) exeCSG_DrillHolePrs::Instance(const Handle(ActAPI_INode)& N)
{
  return new exeCSG_DrillHolePrs(N);
}

//-----------------------------------------------------------------------------

//! Returns true if the Presentation is visible, false -- otherwise.
//! \return true/false.
bool exeCSG_DrillHolePrs::IsVisible() const
{
  return true;
}

//-----------------------------------------------------------------------------

//! Callback for initialization of Presentation pipelines.
void exeCSG_DrillHolePrs::beforeInitPipelines()
{
  // Do nothing...
}

//-----------------------------------------------------------------------------

//! Callback for initialization of Presentation pipelines.
void exeCSG_DrillHolePrs::afterInitPipelines()
{
  // Do nothing...
}

//-----------------------------------------------------------------------------

//! Callback for updating of Presentation pipelines invoked before the
//! kernel update routine starts.
void exeCSG_DrillHolePrs::beforeUpdatePipelines() const
{
  // Do nothing...
}

//-----------------------------------------------------------------------------

//! Callback for updating of Presentation pipelines invoked after the
//! kernel update routine completes.
void exeCSG_DrillHolePrs::afterUpdatePipelines() const
{
  // Do nothing...
}

//-----------------------------------------------------------------------------

//! Callback for highlighting.
//! \param theRenderer  [in] renderer.
//! \param thePickRes   [in] picking results.
//! \param theSelNature [in] selection nature (picking or detecting).
void exeCSG_DrillHolePrs::highlight(vtkRenderer*                  theRenderer,
                                    const asiVisu_PickResult&     thePickRes,
                                    const asiVisu_SelectionNature theSelNature) const
{
  asiVisu_NotUsed(theRenderer);
  asiVisu_NotUsed(thePickRes);
  asiVisu_NotUsed(theSelNature);
}

//-----------------------------------------------------------------------------

//! Callback for highlighting reset.
//! \param theRenderer [in] renderer.
void exeCSG_DrillHolePrs::unHighlight(vtkRenderer*                  theRenderer,
                                      const asiVisu_SelectionNature theSelNature) const
{
  asiVisu_NotUsed(theRenderer);
  asiVisu_NotUsed(theSelNature);
}

//-----------------------------------------------------------------------------

//! Callback for rendering.
//! \param theRenderer [in] renderer.
void exeCSG_DrillHolePrs::renderPipelines(vtkRenderer* theRenderer) const
{
  asiVisu_NotUsed(theRenderer);
}

//-----------------------------------------------------------------------------

//! Callback for de-rendering.
//! \param theRenderer [in] renderer.
void exeCSG_DrillHolePrs::deRenderPipelines(vtkRenderer* theRenderer) const
{
  asiVisu_NotUsed(theRenderer);
}
