//-----------------------------------------------------------------------------
// Created on: 27 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_WorkpiecePrs_h
#define exeCSG_WorkpiecePrs_h

// exeCSG includes
#include <exeCSG_WorkpieceNode.h>

// asiVisu includes
#include <asiVisu_Prs.h>
#include <asiVisu_Utils.h>

//! Presentation class for Workpiece Node.
class exeCSG_WorkpiecePrs : public asiVisu_Prs
{
public:

  // OCCT RTTI
  DEFINE_STANDARD_RTTI_INLINE(exeCSG_WorkpiecePrs, asiVisu_Prs)

  // Allows to register this Presentation class
  DEFINE_PRESENTATION_FACTORY(exeCSG_WorkpieceNode, Instance)

public:

  //! Pipelines.
  enum PipelineId
  {
    Pipeline_Main = 1 //!< Main pipeline.
  };

public:

  static Handle(asiVisu_Prs)
    Instance(const Handle(ActAPI_INode)& N);

  virtual bool
    IsVisible() const;

private:

  //! Allocation is allowed only via Instance method.
  exeCSG_WorkpiecePrs(const Handle(ActAPI_INode)& N);

// Callbacks:
private:

  virtual void beforeInitPipelines();
  virtual void afterInitPipelines();
  virtual void beforeUpdatePipelines() const;
  virtual void afterUpdatePipelines() const;
  virtual void highlight(vtkRenderer* theRenderer,
                         const asiVisu_PickResult& thePickRes,
                         const asiVisu_SelectionNature theSelNature) const;
  virtual void unHighlight(vtkRenderer* theRenderer,
                           const asiVisu_SelectionNature theSelNature) const;
  virtual void renderPipelines(vtkRenderer* theRenderer) const;
  virtual void deRenderPipelines(vtkRenderer* theRenderer) const;

};

#endif
