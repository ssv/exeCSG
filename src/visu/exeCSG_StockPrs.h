//-----------------------------------------------------------------------------
// Created on: 26 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_StockPrs_h
#define exeCSG_StockPrs_h

// exeCSG includes
#include <exeCSG_StockNode.h>

// asiVisu includes
#include <asiVisu_Prs.h>
#include <asiVisu_Utils.h>

//! Presentation class for Stock Node.
class exeCSG_StockPrs : public asiVisu_Prs
{
public:

  // OCCT RTTI
  DEFINE_STANDARD_RTTI_INLINE(exeCSG_StockPrs, asiVisu_Prs)

  // Allows to register this Presentation class
  DEFINE_PRESENTATION_FACTORY(exeCSG_StockNode, Instance)

public:

  //! Pipelines.
  enum PipelineId
  {
    Pipeline_Main = 1, //!< Main pipeline.
    Pipeline_Contour   //!< Contour pipeline.
  };

public:

  static Handle(asiVisu_Prs)
    Instance(const Handle(ActAPI_INode)& N);

  virtual bool
    IsVisible() const;

private:

  //! Allocation is allowed only via Instance method.
  exeCSG_StockPrs(const Handle(ActAPI_INode)& N);

// Callbacks:
private:

  virtual void beforeInitPipelines();
  virtual void afterInitPipelines();
  virtual void beforeUpdatePipelines() const;
  virtual void afterUpdatePipelines() const;
  virtual void highlight(vtkRenderer* theRenderer,
                         const asiVisu_PickResult& thePickRes,
                         const asiVisu_SelectionNature theSelNature) const;
  virtual void unHighlight(vtkRenderer* theRenderer,
                           const asiVisu_SelectionNature theSelNature) const;
  virtual void renderPipelines(vtkRenderer* theRenderer) const;
  virtual void deRenderPipelines(vtkRenderer* theRenderer) const;

};

#endif
