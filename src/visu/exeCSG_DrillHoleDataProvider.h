//-----------------------------------------------------------------------------
// Created on: 27 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_DrillHoleDataProvider_h
#define exeCSG_DrillHoleDataProvider_h

// exeCSG includes
#include <exeCSG_DrillHoleNode.h>

// asiVisu includes
#include <asiVisu_ShapeDataProvider.h>

//! Data provider for a shape representing a drill hole model.
class exeCSG_DrillHoleDataProvider : public asiVisu_ShapeDataProvider
{
public:

  // OCCT RTTI
  DEFINE_STANDARD_RTTI_INLINE(exeCSG_DrillHoleDataProvider, asiVisu_ShapeDataProvider)

public:

  exeCSG_DrillHoleDataProvider(const Handle(exeCSG_DrillHoleNode)& N);

public:

  virtual TopoDS_Shape
    GetShape() const;

};

#endif
