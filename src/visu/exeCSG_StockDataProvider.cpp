//-----------------------------------------------------------------------------
// Created on: 26 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_StockDataProvider.h>

//-----------------------------------------------------------------------------

//! Creates data provider for the given Node.
//! \param N [in] source Node.
exeCSG_StockDataProvider::exeCSG_StockDataProvider(const Handle(exeCSG_StockNode)& N)
: asiVisu_ShapeDataProvider(N->GetId(), NULL) // List of Parameters is gathered just after
{
  m_params = ( ActParamStream() << N->Parameter(exeCSG_StockNode::PID_Shape) ).List;
}

//-----------------------------------------------------------------------------

//! \return shape to visualize.
TopoDS_Shape exeCSG_StockDataProvider::GetShape() const
{
  return ActParamTool::AsShape( m_params->Value(1) )->GetShape();
}
