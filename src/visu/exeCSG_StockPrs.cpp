//-----------------------------------------------------------------------------
// Created on: 26 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_StockPrs.h>

// exeCSG includes
#include <exeCSG_StockDataProvider.h>

// asiVisu includes
#include <asiVisu_ShapePipeline.h>

// VTK includes
#include <vtkMapper.h>
#include <vtkProperty.h>

//-----------------------------------------------------------------------------

//! Creates a Presentation object.
//! \param N [in] Data Node to create a Presentation for.
exeCSG_StockPrs::exeCSG_StockPrs(const Handle(ActAPI_INode)& N)
: asiVisu_Prs(N)
{
  // Create pipeline for phase cylinder
  Handle(asiVisu_ShapePipeline) pl = new asiVisu_ShapePipeline;
  //
  Handle(exeCSG_StockDataProvider)
    dp = new exeCSG_StockDataProvider( Handle(exeCSG_StockNode)::DownCast(N) );
  //
  this->addPipeline       (Pipeline_Main, pl);
  this->assignDataProvider(Pipeline_Main, dp);

  // Create pipeline for edges
  Handle(asiVisu_ShapePipeline)
    contour_pl = new asiVisu_ShapePipeline(true);
  //
  contour_pl->GetDisplayModeFilter()->SetDisplayMode(ShapeDisplayMode_Wireframe);
  //
  contour_pl->Actor()->GetProperty()->SetOpacity(0.75);
  contour_pl->Actor()->GetProperty()->SetLineWidth(2.0f);
  contour_pl->Actor()->SetPickable(0);
  //
  this->addPipeline       (Pipeline_Contour, contour_pl);
  this->assignDataProvider(Pipeline_Contour, dp);
}

//-----------------------------------------------------------------------------

//! Factory method for Presentation.
//! \param N [in] Node to create a Presentation for.
//! \return new Presentation instance.
Handle(asiVisu_Prs) exeCSG_StockPrs::Instance(const Handle(ActAPI_INode)& N)
{
  return new exeCSG_StockPrs(N);
}

//-----------------------------------------------------------------------------

//! Returns true if the Presentation is visible, false -- otherwise.
//! \return true/false.
bool exeCSG_StockPrs::IsVisible() const
{
  return true;
}

//-----------------------------------------------------------------------------

//! Callback for initialization of Presentation pipelines.
void exeCSG_StockPrs::beforeInitPipelines()
{
  // Do nothing...
}

//-----------------------------------------------------------------------------

//! Callback for initialization of Presentation pipelines.
void exeCSG_StockPrs::afterInitPipelines()
{
  // Do nothing...
}

//-----------------------------------------------------------------------------

//! Callback for updating of Presentation pipelines invoked before the
//! kernel update routine starts.
void exeCSG_StockPrs::beforeUpdatePipelines() const
{
  // Do nothing...
}

//-----------------------------------------------------------------------------

//! Callback for updating of Presentation pipelines invoked after the
//! kernel update routine completes.
void exeCSG_StockPrs::afterUpdatePipelines() const
{
  // Do nothing...
}

//-----------------------------------------------------------------------------

//! Callback for highlighting.
//! \param theRenderer  [in] renderer.
//! \param thePickRes   [in] picking results.
//! \param theSelNature [in] selection nature (picking or detecting).
void exeCSG_StockPrs::highlight(vtkRenderer*                  theRenderer,
                                const asiVisu_PickResult&     thePickRes,
                                const asiVisu_SelectionNature theSelNature) const
{
  asiVisu_NotUsed(theRenderer);
  asiVisu_NotUsed(thePickRes);
  asiVisu_NotUsed(theSelNature);
}

//-----------------------------------------------------------------------------

//! Callback for highlighting reset.
//! \param theRenderer [in] renderer.
void exeCSG_StockPrs::unHighlight(vtkRenderer*                  theRenderer,
                                  const asiVisu_SelectionNature theSelNature) const
{
  asiVisu_NotUsed(theRenderer);
  asiVisu_NotUsed(theSelNature);
}

//-----------------------------------------------------------------------------

//! Callback for rendering.
//! \param theRenderer [in] renderer.
void exeCSG_StockPrs::renderPipelines(vtkRenderer* theRenderer) const
{
  asiVisu_NotUsed(theRenderer);
}

//-----------------------------------------------------------------------------

//! Callback for de-rendering.
//! \param theRenderer [in] renderer.
void exeCSG_StockPrs::deRenderPipelines(vtkRenderer* theRenderer) const
{
  asiVisu_NotUsed(theRenderer);
}
