//-----------------------------------------------------------------------------
// Created on: 27 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_DrillHoleDataProvider.h>

//-----------------------------------------------------------------------------

//! Creates data provider for the given Node.
//! \param N [in] source Node.
exeCSG_DrillHoleDataProvider::exeCSG_DrillHoleDataProvider(const Handle(exeCSG_DrillHoleNode)& N)
: asiVisu_ShapeDataProvider(N->GetId(), NULL) // List of Parameters is gathered just after
{
  m_params = ( ActParamStream() << N->Parameter(exeCSG_DrillHoleNode::PID_Shape) ).List;
}

//-----------------------------------------------------------------------------

//! \return shape to visualize.
TopoDS_Shape exeCSG_DrillHoleDataProvider::GetShape() const
{
  return ActParamTool::AsShape( m_params->Value(1) )->GetShape();
}
