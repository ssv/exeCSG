//-----------------------------------------------------------------------------
// Created on: 29 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_h
#define exeCSG_h

#define exeCSG_NotUsed(x)

// asiData includes
#include <asiData.h>

//-----------------------------------------------------------------------------
// Custom Active Data Parameters
//-----------------------------------------------------------------------------

#define Parameter_FeatureFaces Parameter_LASTFREE_ASITUS

#endif
