//-----------------------------------------------------------------------------
// Created on: 22 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_Controls_h
#define exeCSG_Controls_h

// Qt includes
#pragma warning(push, 0)
#include <QPushButton>
#include <QVBoxLayout>
#pragma warning(pop)

//! Widget for sheet metal controls.
class exeCSG_Controls : public QWidget
{
  Q_OBJECT

public:

  exeCSG_Controls(QWidget* parent = NULL);
  virtual ~exeCSG_Controls();

public slots:

  void onInitStock ();
  void onShowGraph ();
  void onDumpOCAF  ();

private:

  QVBoxLayout* m_pMainLayout; //!< Layout of the widget.

  //! Widgets.
  struct t_widgets
  {
    QPushButton* pInitStock; //!< Initializes stock.
    QPushButton* pShowGraph; //!< Shows construction graph.
    QPushButton* pDumpOCAF;  //!< Dumps OCAF structure to text view.

    t_widgets() : pInitStock (NULL),
                  pShowGraph (NULL),
                  pDumpOCAF  (NULL)
    {}

    void Release()
    {
      delete pInitStock; pInitStock = NULL;
      delete pShowGraph; pShowGraph = NULL;
      delete pDumpOCAF;  pDumpOCAF  = NULL;
    }
  };

  //! Involved widgets.
  t_widgets m_widgets;

};

#endif
