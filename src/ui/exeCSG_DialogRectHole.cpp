//-----------------------------------------------------------------------------
// Created on: 23 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_DialogRectHole.h>

// exeCSG includes
#include <exeCSG_CommonFacilities.h>
#include <exeCSG_Commands.h>

// asiUI includes
#include <asiUI_Common.h>

// asiAlgo includes
#include <asiAlgo_Timer.h>

// Qt includes
#include <QGroupBox>
#include <QLabel>
#include <QSizePolicy>

//-----------------------------------------------------------------------------

#define CONTROL_EDIT_WIDTH 100
#define CONTROL_BTN_WIDTH 150

//-----------------------------------------------------------------------------
// Construction & destruction
//-----------------------------------------------------------------------------

//! Constructor.
//! \param placement [in] placement axes for the feature being created.
//! \param parent    [in] parent widget.
exeCSG_DialogRectHole::exeCSG_DialogRectHole(const gp_Ax3& placement,
                                             QWidget*      parent)
: QDialog(parent), m_placement(placement)
{
  // Main layout
  m_pMainLayout = new QVBoxLayout();

  // Group box for parameters
  QGroupBox* pGroup = new QGroupBox("Parameters");

  // Editors
  m_widgets.pWidth  = new asiUI_LineEdit();
  m_widgets.pHeight = new asiUI_LineEdit();
  m_widgets.pDepth  = new asiUI_LineEdit();

  // Sizing
  m_widgets.pWidth  ->setMinimumWidth(CONTROL_EDIT_WIDTH);
  m_widgets.pHeight ->setMinimumWidth(CONTROL_EDIT_WIDTH);
  m_widgets.pDepth  ->setMinimumWidth(CONTROL_EDIT_WIDTH);

  //---------------------------------------------------------------------------
  // Buttons
  //---------------------------------------------------------------------------

  m_widgets.pProceed = new QPushButton("Proceed");

  // Sizing
  m_widgets.pProceed->setMaximumWidth(CONTROL_BTN_WIDTH);

  // Reaction
  connect( m_widgets.pProceed, SIGNAL( clicked() ), this, SLOT( onProceed() ) );

  //---------------------------------------------------------------------------
  // Line editors
  //---------------------------------------------------------------------------

  // Create layout
  QGridLayout* pGrid = new QGridLayout(pGroup);
  //
  pGrid->addWidget(new QLabel("Width:"),  0, 0);
  pGrid->addWidget(new QLabel("Height:"), 1, 0);
  pGrid->addWidget(new QLabel("Depth:"),  2, 0);
  //
  pGrid->addWidget(m_widgets.pWidth,  0, 1);
  pGrid->addWidget(m_widgets.pHeight, 1, 1);
  pGrid->addWidget(m_widgets.pDepth,  2, 1);
  //
  pGrid->setSpacing(5);
  pGrid->setColumnStretch(0, 0);
  pGrid->setColumnStretch(1, 1);
  pGrid->setAlignment(Qt::AlignTop | Qt::AlignLeft);

  //---------------------------------------------------------------------------
  // Main layout
  //---------------------------------------------------------------------------

  // Configure main layout
  m_pMainLayout->addWidget(pGroup);
  m_pMainLayout->addWidget(m_widgets.pProceed);
  m_pMainLayout->setAlignment(Qt::AlignTop | Qt::AlignLeft);
  m_pMainLayout->setContentsMargins(10, 10, 10, 10);

  this->setLayout(m_pMainLayout);
  this->setWindowModality(Qt::WindowModal);
  this->setWindowTitle("Rectangular hole");

  //---------------------------------------------------------------------------
  // Initialize
  //---------------------------------------------------------------------------

  // Set to UI controls
  m_widgets.pWidth  ->setText("1.0");
  m_widgets.pHeight ->setText("1.0");
  m_widgets.pDepth  ->setText("1.0");
}

//-----------------------------------------------------------------------------

//! Destructor.
exeCSG_DialogRectHole::~exeCSG_DialogRectHole()
{
  delete m_pMainLayout;
  m_widgets.Release();
}

//-----------------------------------------------------------------------------

//! Reaction on proceed.
void exeCSG_DialogRectHole::onProceed()
{
  // Convert user input to double
  const double width  = QVariant( m_widgets.pWidth  ->text() ).toDouble();
  const double height = QVariant( m_widgets.pHeight ->text() ).toDouble();
  const double depth  = QVariant( m_widgets.pDepth  ->text() ).toDouble();

  Handle(exeCSG_CommonFacilities) cf = exeCSG_CommonFacilities::Instance();

  // Modify Data Model
  Handle(exeCSG_RectHoleNode) featureNode;
  //
  cf->Model->OpenCommand();
  {
    featureNode = exeCSG_Commands(cf->Model).CreateRectHoleNode(m_placement, width, height, depth);
  }
  cf->Model->CommitCommand();

  // Actualize
  cf->ObjectBrowser->Populate();
  cf->Prs.Stock->Actualize(featureNode, false, false);

  // Close
  this->close();
}
