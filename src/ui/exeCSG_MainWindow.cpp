//-----------------------------------------------------------------------------
// Created on: 22 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_MainWindow.h>

// exeCSG includes
#include <exeCSG_CommonFacilities.h>

// asiUI includes
#include <asiUI_IV.h>
#include <asiUI_StatusBar.h>
#include <asiUI_StatusBarImpl.h>
#include <asiUI_StyledTextEdit.h>

// Qt includes
#pragma warning(push, 0)
#include <QApplication>
#include <QTextStream>
#include <QDesktopWidget>
#include <QDockWidget>
#pragma warning(pop)

//-----------------------------------------------------------------------------
// Construction & destruction
//-----------------------------------------------------------------------------

//! Constructor.
exeCSG_MainWindow::exeCSG_MainWindow() : QMainWindow()
{
  this->createStockViewer();
  this->createDockWindows();

  this->setCentralWidget(m_widgets.wViewerStock);
  this->setWindowTitle("CSG parametric modeling demo");

  //---------------------------------------------------------------------------
  // Apply fantastic dark theme
  //---------------------------------------------------------------------------

  QFile f(":qdarkstyle/style.qss");
  if ( !f.exists() )
  {
    printf("Unable to set stylesheet, file not found\n");
  }
  else
  {
    f.open(QFile::ReadOnly | QFile::Text);
    QTextStream ts(&f);
    qApp->setStyleSheet( ts.readAll() );
  }
}

//-----------------------------------------------------------------------------

//! Destructor.
exeCSG_MainWindow::~exeCSG_MainWindow()
{}

//-----------------------------------------------------------------------------

//! Gets control on window close.
//! \param evt [in] event.
void exeCSG_MainWindow::closeEvent(QCloseEvent* evt)
{
  // It seems that we have to destruct objects properly and manually in
  // order to avoid some side effects from VTK. E.g. if we don't kill the
  // widgets explicitly here, we may sometimes get a warning window of VTK
  // saying that it lacks some resources
  m_widgets.Release();
  //
  evt->accept();
}

//-----------------------------------------------------------------------------

//! Creates main (stock) viewer.
void exeCSG_MainWindow::createStockViewer()
{
  // Common facilities instance
  Handle(exeCSG_CommonFacilities) cf = exeCSG_CommonFacilities::Instance();

  // Create viewer
  m_widgets.wViewerStock = new asiUI_ViewerPart(cf->Model, true);

  // Desktop used for sizing
  QDesktopWidget desktop;
  const int side   = std::min( desktop.height(), desktop.width() );
  const int width  = side*0.25;
  const int height = side*0.25;
  //
  m_widgets.wViewerStock->setMinimumSize(width, height);

  // Initialize desktop
  cf->ViewerStock = m_widgets.wViewerStock;
  cf->Prs.Stock   = m_widgets.wViewerStock->PrsMgr();
}

//-----------------------------------------------------------------------------

//! Creates main dockable widgets.
void exeCSG_MainWindow::createDockWindows()
{
  // Common facilities instance
  Handle(exeCSG_CommonFacilities) cf = exeCSG_CommonFacilities::Instance();

  // Desktop used for sizing
  QDesktopWidget desktop;
  const int side  = std::min( desktop.height(), desktop.width() );
  const int width = side*0.25;

  //---------------------------------------------------------------------------
  // Workpiece viewer
  {
    QDockWidget* pDock = new QDockWidget("Workpiece", this);
    pDock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    //
    m_widgets.wViewerWorkpiece = new asiUI_ViewerPart(cf->Model, pDock);
    pDock->setWidget(m_widgets.wViewerWorkpiece);
    pDock->setMinimumWidth(width);
    //
    this->addDockWidget(Qt::RightDockWidgetArea, pDock);

    // Initialize desktop
    cf->ViewerWorkpiece = m_widgets.wViewerWorkpiece;
    cf->Prs.Workpiece   = m_widgets.wViewerWorkpiece->PrsMgr();
  }
  //---------------------------------------------------------------------------
  // Object browser
  QDockWidget* pDockBrowser;
  {
    pDockBrowser = new QDockWidget("Data", this);
    pDockBrowser->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    //
    m_widgets.wBrowser = new exeCSG_ObjectBrowser(cf->Model, cf->ProgressNotifier, pDockBrowser);
    m_widgets.wBrowser->AddAssociatedViewer(cf->ViewerStock);
    m_widgets.wBrowser->AddAssociatedViewer(cf->ViewerWorkpiece);
    pDockBrowser->setWidget(m_widgets.wBrowser);
    //
    this->addDockWidget(Qt::LeftDockWidgetArea, pDockBrowser);

    // Initialize desktop
    cf->ObjectBrowser = m_widgets.wBrowser;
  }
  //---------------------------------------------------------------------------
  // CSG controls
  QDockWidget* pDockCSG;
  {
    pDockCSG = new QDockWidget("CSG", this);
    pDockCSG->setAllowedAreas(Qt::LeftDockWidgetArea);
    //
    m_widgets.wControlsCSG = new exeCSG_Controls(pDockCSG);
    pDockCSG->setWidget(m_widgets.wControlsCSG);
    //
    this->addDockWidget(Qt::LeftDockWidgetArea, pDockCSG);
  }
  //---------------------------------------------------------------------------
  this->tabifyDockWidget(pDockBrowser, pDockCSG);

  // Now we have everything to initialize imperative plotters
  cf->PlotterStock     = new asiUI_IV(cf->Model, cf->Prs.Stock,     NULL, cf->ObjectBrowser);
  cf->PlotterWorkpiece = new asiUI_IV(cf->Model, cf->Prs.Workpiece, NULL, cf->ObjectBrowser);

  // Listener for part viewer
  m_listeners.pViewerStock = new exeCSG_ViewerStockListener();

  // Signals-slots
  m_listeners.pViewerStock->Connect();

  // Log window
  QDockWidget* pDockLogWindow;
  {
    pDockLogWindow = new QDockWidget("Logger", this);
    pDockLogWindow->setAllowedAreas(Qt::BottomDockWidgetArea);
    //
    m_widgets.wLogger = new asiUI_StyledTextEdit(pDockLogWindow);
    //
    pDockLogWindow->setWidget(m_widgets.wLogger);
    //
    this->addDockWidget(Qt::BottomDockWidgetArea, pDockLogWindow);
  }

  // Create status bar
  Handle(asiUI_StatusBarImpl)
    statusBar = new asiUI_StatusBarImpl(new asiUI_StatusBar);
  //
  this->setStatusBar( statusBar->GetStatusBar() );
  //
  cf->StatusBar = statusBar;
  cf->StatusBar->SetStatusText("CSG parametric modeling demo");

  // Initialize and connect progress listener
  cf->Logger           = new asiUI_Logger(m_widgets.wLogger);
  cf->ProgressListener = new asiUI_ProgressListener(statusBar, cf->ProgressNotifier, cf->Logger);
  cf->ProgressListener->Connect();
}
