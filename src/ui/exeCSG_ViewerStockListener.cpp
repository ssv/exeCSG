//-----------------------------------------------------------------------------
// Created on: 26 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_ViewerStockListener.h>

// exeCSG includes
#include <exeCSG_Commands.h>
#include <exeCSG_CommonFacilities.h>

// asiVisu includes
#include <asiVisu_NodeInfo.h>

// OCCT includes
#include <BRep_Tool.hxx>
#include <ShapeAnalysis_Surface.hxx>
#include <TopExp.hxx>
#include <TopoDS.hxx>

//-----------------------------------------------------------------------------

//! ctor.
exeCSG_ViewerStockListener::exeCSG_ViewerStockListener()
: asiUI_ViewerPartListener( exeCSG_CommonFacilities::Instance()->ViewerStock,
                            NULL,
                            NULL,
                            exeCSG_CommonFacilities::Instance()->Model,
                            exeCSG_CommonFacilities::Instance()->ProgressNotifier,
                            exeCSG_CommonFacilities::Instance()->PlotterStock )
{}

//-----------------------------------------------------------------------------

//! Destructor.
exeCSG_ViewerStockListener::~exeCSG_ViewerStockListener() {}

//-----------------------------------------------------------------------------

//! Connects this listener to the target widget.
void exeCSG_ViewerStockListener::Connect()
{
  // Common facilities instance
  Handle(exeCSG_CommonFacilities) cf = exeCSG_CommonFacilities::Instance();

  connect( cf->ViewerStock, SIGNAL ( facePicked(const asiVisu_PickResult&) ),
           this,            SLOT   ( onFacePicked(const asiVisu_PickResult&) ) );
}

//-----------------------------------------------------------------------------

//! Reaction to face picking.
//! \param pick_res [in] picking result.
void exeCSG_ViewerStockListener::onFacePicked(const asiVisu_PickResult& pick_res)
{
  // Common facilities instance
  Handle(exeCSG_CommonFacilities) cf = exeCSG_CommonFacilities::Instance();
  //
  const vtkSmartPointer<vtkActor>&  picked_actor  = pick_res.GetPickedActor();
  const TColStd_PackedMapOfInteger& subshape_mask = pick_res.GetPickedElementIds();

  double px, py, pz;
  pick_res.GetPickedPos(px, py, pz);
  gp_Pnt P(px, py, pz);

  if ( !picked_actor.GetPointer() )
    return; // Nothing selected

  // Retrieve Node information
  asiVisu_NodeInfo* pNodeInfo = asiVisu_NodeInfo::Retrieve(picked_actor);
  //
  if ( !pNodeInfo )
    return;

  // Check if the picked Node is a Stock Node
  Handle(ActAPI_INode) node = cf->Model->FindNode( pNodeInfo->GetNodeId() );
  //
  if ( !node->IsInstance( STANDARD_TYPE(exeCSG_StockNode) ) )
    return;

  /* =================
   *  Get picked face
   * ================= */

  Handle(exeCSG_StockNode) stockNode  = Handle(exeCSG_StockNode)::DownCast(node);
  TopoDS_Shape             stockShape = stockNode->GetShape();

  // Build topology map.
  // WARNING: in production code, this should not be repeated on every click.
  TopTools_IndexedMapOfShape allSubShapes;
  TopExp::MapShapes(stockShape, allSubShapes);
  //
  const TopoDS_Face& face = TopoDS::Face( allSubShapes( subshape_mask.GetMinimalMapped() ) );

  /* =======================
   *  Derive placement axes
   * ======================= */

  Handle(Geom_Surface) surf = BRep_Tool::Surface(face);

  // Project point on surface
  ShapeAnalysis_Surface sas(surf);
  gp_Pnt2d UV = sas.ValueOfUV(P, 1.0e-6);
  //
  if ( sas.Gap() > 0.1 )
  {
    m_progress.SendLogMessage(LogWarn(Normal) << "Picked point is too far from the phase cylinder");
    return;
  }

  // N (Vz)
  gp_Vec D1U, D1V;
  surf->D1(UV.X(), UV.Y(), P, D1U, D1V);
  //
  gp_Vec N = D1U.Crossed(D1V);
  if ( N.Magnitude() < RealEpsilon() )
  {
    m_progress.SendLogMessage(LogWarn(Normal) << "Irregular surface parameterization");
    return;
  }
  //
  if ( face.Orientation() == TopAbs_REVERSED )
    N.Reverse();

  // Construct feature placement
  gp_Ax3 featurePlacement( P, N.Reversed() );

  /* ===========================
   *  Create Data Model feature
   * =========================== */

  exeCSG_Commands API(cf->Model);

  // Modify Data Model
  Handle(exeCSG_DrillHoleNode) featureNode;
  //
  cf->Model->OpenCommand();
  {
    featureNode = API.CreateDrillHoleNode( featurePlacement, 2.0, 1.5 );
    //
    cf->Model->FuncExecuteAll();
  }
  cf->Model->CommitCommand();

  /////////////////////////////////////////////////////////////////////////////
  cf->PlotterStock->DRAW_POINT(P, Color_Blue, "picked pos");
  /////////////////////////////////////////////////////////////////////////////

  cf->ViewerStock->PrsMgr()->Actualize( featureNode );
  cf->ViewerWorkpiece->PrsMgr()->Actualize( cf->Model->GetWorkpieceNode(), false, true );
}
