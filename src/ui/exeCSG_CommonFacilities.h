//-----------------------------------------------------------------------------
// Created on: 22 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_CommonFacilities_h
#define exeCSG_CommonFacilities_h

// exeCSG includes
#include <exeCSG_Model.h>
#include <exeCSG_ObjectBrowser.h>

// asiUI includes
#include <asiUI_IV.h>
#include <asiUI_ProgressListener.h>
#include <asiUI_ProgressNotifier.h>
#include <asiUI_ViewerDomain.h>
#include <asiUI_ViewerHost.h>
#include <asiUI_ViewerPart.h>

// asiVisu includes
#include <asiVisu_PrsManager.h>

DEFINE_STANDARD_HANDLE(exeCSG_CommonFacilities, Standard_Transient)

//! Common tools and objects for the application.
class exeCSG_CommonFacilities : public Standard_Transient
{
public:

  // OCCT RTTI
  DEFINE_STANDARD_RTTI_INLINE(exeCSG_CommonFacilities, Standard_Transient)

public:

  Handle(exeCSG_Model)             Model;            //!< Data Model instance.
  //
  exeCSG_ObjectBrowser*            ObjectBrowser;    //!< Object browser.
  //
  asiUI_ViewerPart*                ViewerStock;      //!< Viewer for part.
  asiUI_ViewerPart*                ViewerWorkpiece;  //!< Viewer for face parametric domain.
  //
  Handle(ActAPI_IProgressNotifier) ProgressNotifier; //!< Progress notifier.
  asiUI_ProgressListener*          ProgressListener; //!< Progress listener.
  Handle(ActAPI_IPlotter)          PlotterStock;     //!< Imperative plotter for Stock viewer.
  Handle(ActAPI_IPlotter)          PlotterWorkpiece; //!< Imperative plotter for Workpiece viewer.
  Handle(asiUI_IStatusBar)         StatusBar;        //!< Status bar of the main window.
  Handle(asiUI_Logger)             Logger;           //!< Logger.

  //! Visualization facilities.
  struct t_prs
  {
  //---------------------------------------------------------------------------
    vtkSmartPointer<asiVisu_PrsManager> Stock;     //!< Stock part.
    vtkSmartPointer<asiVisu_PrsManager> Workpiece; //!< Workpiece part.
  //---------------------------------------------------------------------------

    void ActualizeAll()
    {
      if ( Stock )     Stock     ->Actualize(exeCSG_CommonFacilities::Instance()->Model->GetRootNode(), true);
      if ( Workpiece ) Workpiece ->Actualize(exeCSG_CommonFacilities::Instance()->Model->GetRootNode(), true);
    }

    void DeleteAll()
    {
      if ( Stock )     Stock     ->DeleteAllPresentations();
      if ( Workpiece ) Workpiece ->DeleteAllPresentations();
    }
  } Prs;

public:

  //! \return single instance of facilities.
  static Handle(exeCSG_CommonFacilities) Instance()
  {
    if ( m_ref.IsNull() )
      m_ref = new exeCSG_CommonFacilities;

    return m_ref;
  }

private:

  exeCSG_CommonFacilities()
  //
    : ObjectBrowser    (NULL),
      ViewerStock      (NULL),
      ViewerWorkpiece  (NULL),
      ProgressListener (NULL)
  {
    // Create Data Model
    Model = new exeCSG_Model;
    if ( !Model->NewEmpty() )
    {
      Standard_ProgramError::Raise("Cannot create Data Model");
    }
    //
    Model->DisableTransactions();
    {
      Model->Populate();
    }
    Model->EnableTransactions();

    // Initialize notifier
    ProgressNotifier = new asiUI_ProgressNotifier;
  }

private:

  static Handle(exeCSG_CommonFacilities) m_ref; //!< Single instance of facilities.

};

#endif
