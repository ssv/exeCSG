//-----------------------------------------------------------------------------
// Created on: 29 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_ObjectBrowser.h>

// exeCSG includes
#include <exeCSG_CommonFacilities.h>
#include <exeCSG_DialogDrillHole.h>

// Qt includes
#include <QMenu>

//-----------------------------------------------------------------------------

//! Creates a new instance of tree view.
//! \param model    [in] Data Model instance.
//! \param progress [in] progress notifier.
//! \param parent   [in] parent widget.
exeCSG_ObjectBrowser::exeCSG_ObjectBrowser(const Handle(ActAPI_IModel)& model,
                                           ActAPI_ProgressEntry         progress,
                                           QWidget*                     parent)
: asiUI_ObjectBrowser(model, progress, parent)
{}

//-----------------------------------------------------------------------------

//! Destructor.
exeCSG_ObjectBrowser::~exeCSG_ObjectBrowser()
{}

//-----------------------------------------------------------------------------

//! Populates context menu with actions.
//! \param activeNode [in]      currently active Node.
//! \param pMenu      [in, out] menu to populate.
void exeCSG_ObjectBrowser::populateContextMenu(const Handle(ActAPI_INode)& activeNode,
                                               QMenu*                      pMenu)
{
  asiUI_ObjectBrowser::populateContextMenu(activeNode, pMenu);

  // Customization
  if ( activeNode->IsKind( STANDARD_TYPE(exeCSG_DrillHoleNode) ) )
  {
    pMenu->addSeparator();
    pMenu->addAction( "Edit", this, SLOT( onEditDrillHole() ) );
  }
}

//-----------------------------------------------------------------------------

//! Reaction on "edit" action.
void exeCSG_ObjectBrowser::onEditDrillHole()
{
  Handle(ActAPI_INode) selected_n;
  if ( !this->selectedNode(selected_n) ) return;

  exeCSG_DialogDrillHole*
    pDlg = new exeCSG_DialogDrillHole( Handle(exeCSG_DrillHoleNode)::DownCast(selected_n), this );
  //
  pDlg->show();
}
