//-----------------------------------------------------------------------------
// Created on: 22 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_MainWindow_h
#define exeCSG_MainWindow_h

// exeCSG includes
#include <exeCSG_Controls.h>
#include <exeCSG_ObjectBrowser.h>
#include <exeCSG_ViewerStockListener.h>

// asiUI includes
#include <asiUI_ViewerPart.h>
#include <asiUI_ViewerPartListener.h>

// Qt includes
#pragma warning(push, 0)
#include <QCloseEvent>
#include <QMainWindow>
#include <QTextEdit>
#pragma warning(pop)

//! Main window.
class exeCSG_MainWindow : public QMainWindow
{
  Q_OBJECT

public:

  exeCSG_MainWindow();
  virtual ~exeCSG_MainWindow();

public:

  void closeEvent(QCloseEvent* evt);

private:

  void createStockViewer();
  void createDockWindows();

private:

  //! Widgets.
  struct t_widgets
  {
    exeCSG_ObjectBrowser* wBrowser;         //!< Object browser.
    asiUI_ViewerPart*     wViewerStock;     //!< Stock viewer.
    asiUI_ViewerPart*     wViewerWorkpiece; //!< Workpiece viewer.
    exeCSG_Controls*      wControlsCSG;     //!< Controls for CSG demo.
    QTextEdit*            wLogger;          //!< Logger.

    t_widgets() : wBrowser         (NULL),
                  wViewerStock     (NULL),
                  wViewerWorkpiece (NULL),
                  wControlsCSG     (NULL),
                  wLogger          (NULL)
    {}

    void Release()
    {
      delete wBrowser;         wBrowser         = NULL;
      delete wViewerStock;     wViewerStock     = NULL;
      delete wViewerWorkpiece; wViewerWorkpiece = NULL;
      delete wControlsCSG;     wControlsCSG     = NULL;
      delete wLogger;          wLogger          = NULL;
    }
  };

  //! Listeners.
  struct t_listeners
  {
    exeCSG_ViewerStockListener* pViewerStock;  //!< Listener for part viewer.

    t_listeners() : pViewerStock  (NULL)
    {}

    void Release()
    {
      delete pViewerStock;  pViewerStock  = NULL;
    }
  };

  t_widgets   m_widgets;
  t_listeners m_listeners;

};

#endif
