//-----------------------------------------------------------------------------
// Created on: 23 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_DialogInitStock_h
#define exeCSG_DialogInitStock_h

// asiUI includes
#include <asiUI_LineEdit.h>

// Qt includes
#pragma warning(push, 0)
#include <QDialog>
#include <QPushButton>
#include <QVBoxLayout>
#pragma warning(pop)

//! Controls to set stock shape parameters.
class exeCSG_DialogInitStock : public QDialog
{
  Q_OBJECT

public:

  exeCSG_DialogInitStock(QWidget* parent = NULL);

  virtual ~exeCSG_DialogInitStock();

public slots:

  void onProceed();

protected:

  //! Widgets.
  struct t_widgets
  {
  //---------------------------------------------------------------------------
    QPushButton*    pProceed; //!< Proceed with constructing hole.
  //---------------------------------------------------------------------------
    asiUI_LineEdit* pLeftX;   //!< Left X coordinate.
    asiUI_LineEdit* pLeftY;   //!< Left Y coordinate.
    asiUI_LineEdit* pLeftZ;   //!< Left Z coordinate.
    asiUI_LineEdit* pRightX;  //!< Right X coordinate.
    asiUI_LineEdit* pRightY;  //!< Right Y coordinate.
    asiUI_LineEdit* pRightZ;  //!< Right Z coordinate.
  //---------------------------------------------------------------------------

    t_widgets() : pProceed (NULL),
                  pLeftX   (NULL),
                  pLeftY   (NULL),
                  pLeftZ   (NULL),
                  pRightX  (NULL),
                  pRightY  (NULL),
                  pRightZ  (NULL)
    {}

    void Release()
    {
      delete pProceed; pProceed = NULL;
      delete pLeftX;   pLeftX   = NULL;
      delete pLeftY;   pLeftY   = NULL;
      delete pLeftZ;   pLeftZ   = NULL;
      delete pRightX;  pRightX  = NULL;
      delete pRightY;  pRightY  = NULL;
      delete pRightZ;  pRightZ  = NULL;
    }
  };

  t_widgets    m_widgets;     //!< UI controls.
  QVBoxLayout* m_pMainLayout; //!< Layout of the widget.

};

#endif
