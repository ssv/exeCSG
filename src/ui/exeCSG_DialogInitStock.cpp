//-----------------------------------------------------------------------------
// Created on: 23 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_DialogInitStock.h>

// exeCSG includes
#include <exeCSG_CommonFacilities.h>
#include <exeCSG_Commands.h>

// asiUI includes
#include <asiUI_Common.h>

// asiAlgo includes
#include <asiAlgo_Timer.h>

// Qt includes
#include <QGroupBox>
#include <QLabel>
#include <QSizePolicy>

//-----------------------------------------------------------------------------

#define CONTROL_EDIT_WIDTH 100
#define CONTROL_BTN_WIDTH 150

//-----------------------------------------------------------------------------
// Construction & destruction
//-----------------------------------------------------------------------------

//! Constructor.
//! \param parent [in] parent widget.
exeCSG_DialogInitStock::exeCSG_DialogInitStock(QWidget* parent)
: QDialog(parent)
{
  // Main layout
  m_pMainLayout = new QVBoxLayout();

  // Group box for parameters
  QGroupBox* pGroup = new QGroupBox("Parameters");

  // Editors
  m_widgets.pLeftX  = new asiUI_LineEdit();
  m_widgets.pLeftY  = new asiUI_LineEdit();
  m_widgets.pLeftZ  = new asiUI_LineEdit();
  m_widgets.pRightX = new asiUI_LineEdit();
  m_widgets.pRightY = new asiUI_LineEdit();
  m_widgets.pRightZ = new asiUI_LineEdit();

  // Sizing
  m_widgets.pLeftX  ->setMinimumWidth(CONTROL_EDIT_WIDTH);
  m_widgets.pLeftY  ->setMinimumWidth(CONTROL_EDIT_WIDTH);
  m_widgets.pLeftZ  ->setMinimumWidth(CONTROL_EDIT_WIDTH);
  m_widgets.pRightX ->setMinimumWidth(CONTROL_EDIT_WIDTH);
  m_widgets.pRightY ->setMinimumWidth(CONTROL_EDIT_WIDTH);
  m_widgets.pRightZ ->setMinimumWidth(CONTROL_EDIT_WIDTH);

  //---------------------------------------------------------------------------
  // Buttons
  //---------------------------------------------------------------------------

  m_widgets.pProceed = new QPushButton("Proceed");

  // Sizing
  m_widgets.pProceed->setMaximumWidth(CONTROL_BTN_WIDTH);

  // Reaction
  connect( m_widgets.pProceed, SIGNAL( clicked() ), this, SLOT( onProceed() ) );

  //---------------------------------------------------------------------------
  // Line editors
  //---------------------------------------------------------------------------

  // Create layout
  QGridLayout* pGrid = new QGridLayout(pGroup);
  //
  pGrid->addWidget(new QLabel("Left X:"),  0, 0);
  pGrid->addWidget(new QLabel("Left Y:"),  1, 0);
  pGrid->addWidget(new QLabel("Left Z:"),  2, 0);
  pGrid->addWidget(new QLabel("Right X:"), 3, 0);
  pGrid->addWidget(new QLabel("Right Y:"), 4, 0);
  pGrid->addWidget(new QLabel("Right Z:"), 5, 0);
  //
  pGrid->addWidget(m_widgets.pLeftX,  0, 1);
  pGrid->addWidget(m_widgets.pLeftY,  1, 1);
  pGrid->addWidget(m_widgets.pLeftZ,  2, 1);
  pGrid->addWidget(m_widgets.pRightX, 3, 1);
  pGrid->addWidget(m_widgets.pRightY, 4, 1);
  pGrid->addWidget(m_widgets.pRightZ, 5, 1);
  //
  pGrid->setSpacing(5);
  pGrid->setColumnStretch(0, 0);
  pGrid->setColumnStretch(1, 1);
  pGrid->setAlignment(Qt::AlignTop | Qt::AlignLeft);

  //---------------------------------------------------------------------------
  // Main layout
  //---------------------------------------------------------------------------

  // Configure main layout
  m_pMainLayout->addWidget(pGroup);
  m_pMainLayout->addWidget(m_widgets.pProceed);
  m_pMainLayout->setAlignment(Qt::AlignTop | Qt::AlignLeft);
  m_pMainLayout->setContentsMargins(10, 10, 10, 10);

  this->setLayout(m_pMainLayout);
  this->setWindowModality(Qt::WindowModal);
  this->setWindowTitle("Set parameters of stock model");

  //---------------------------------------------------------------------------
  // Initialize
  //---------------------------------------------------------------------------

  // Set to UI controls
  m_widgets.pLeftX  ->setText("0.0");
  m_widgets.pLeftY  ->setText("0.0");
  m_widgets.pLeftZ  ->setText("0.0");
  m_widgets.pRightX ->setText("40.0");
  m_widgets.pRightY ->setText("30.0");
  m_widgets.pRightZ ->setText("2.5");
}

//! Destructor.
exeCSG_DialogInitStock::~exeCSG_DialogInitStock()
{
  delete m_pMainLayout;
  m_widgets.Release();
}

//-----------------------------------------------------------------------------

//! Reaction on proceed.
void exeCSG_DialogInitStock::onProceed()
{
  // Convert user input to double
  const double left_x  = QVariant( m_widgets.pLeftX  ->text() ).toDouble();
  const double left_y  = QVariant( m_widgets.pLeftY  ->text() ).toDouble();
  const double left_z  = QVariant( m_widgets.pLeftZ  ->text() ).toDouble();
  const double right_x = QVariant( m_widgets.pRightX ->text() ).toDouble();
  const double right_y = QVariant( m_widgets.pRightY ->text() ).toDouble();
  const double right_z = QVariant( m_widgets.pRightZ ->text() ).toDouble();

  Handle(exeCSG_CommonFacilities) cf = exeCSG_CommonFacilities::Instance();

  // Get Stock Node
  Handle(exeCSG_StockNode) stockNode = cf->Model->GetStockNode();

  // Modify Data Model
  cf->Model->OpenCommand();
  {
    stockNode->SetLeftX(left_x);
    stockNode->SetLeftY(left_y);
    stockNode->SetLeftZ(left_z);
    stockNode->SetRightX(right_x);
    stockNode->SetRightY(right_y);
    stockNode->SetRightZ(right_z);

    // Execute Tree Functions
    cf->Model->FuncExecuteAll();
  }
  cf->Model->CommitCommand();

  // Actualize
  cf->ObjectBrowser->Populate();
  cf->Prs.Stock->Actualize(stockNode, false, true);

  // Close
  this->close();
}
