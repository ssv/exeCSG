//-----------------------------------------------------------------------------
// Created on: 29 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_ObjectBrowser_h
#define exeCSG_ObjectBrowser_h

// asiUI includes
#include <asiUI_ObjectBrowser.h>

// Active Data includes
#include <ActAPI_INode.h>

//-----------------------------------------------------------------------------

//! Tree view for CSG demo.
class exeCSG_ObjectBrowser : public asiUI_ObjectBrowser
{
  Q_OBJECT

public:

  exeCSG_ObjectBrowser(const Handle(ActAPI_IModel)& model,
                       ActAPI_ProgressEntry         progress,
                       QWidget*                     parent = NULL);

  virtual
    ~exeCSG_ObjectBrowser();

public:

  virtual void populateContextMenu(const Handle(ActAPI_INode)& activeNode,
                                   QMenu*                      pMenu);

//-----------------------------------------------------------------------------
protected slots:

  void onEditDrillHole();

protected:

  Handle(ActAPI_INode) m_selectedNode; //!< Currently active Data Node.

};

#endif
