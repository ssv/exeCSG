//-----------------------------------------------------------------------------
// Created on: 22 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_GraphItem.h>

// VTK includes
#include <vtkObjectFactory.h>

vtkStandardNewMacro(exeCSG_GraphItem);

//! Destructor.
exeCSG_GraphItem::~exeCSG_GraphItem()
{}
