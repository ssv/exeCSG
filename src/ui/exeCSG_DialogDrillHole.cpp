//-----------------------------------------------------------------------------
// Created on: 22 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_DialogDrillHole.h>

// exeCSG includes
#include <exeCSG_CommonFacilities.h>
#include <exeCSG_Commands.h>

// asiUI includes
#include <asiUI_Common.h>

// asiAlgo includes
#include <asiAlgo_Timer.h>

// Qt includes
#include <QGroupBox>
#include <QLabel>
#include <QSizePolicy>

//-----------------------------------------------------------------------------

#define CONTROL_EDIT_WIDTH 100
#define CONTROL_BTN_WIDTH 150

//-----------------------------------------------------------------------------
// Construction & destruction
//-----------------------------------------------------------------------------

//! Constructor.
//! \param placement [in] placement axes for the feature being created.
//! \param parent    [in] parent widget.
exeCSG_DialogDrillHole::exeCSG_DialogDrillHole(const gp_Ax3& placement,
                                               QWidget*      parent)
: QDialog(parent), m_placement(placement)
{
  this->construct();
}

//-----------------------------------------------------------------------------

//! Constructor accepting the Node to edit.
//! \param N [in] Node to edit.
//! \param parent    [in] parent widget.
exeCSG_DialogDrillHole::exeCSG_DialogDrillHole(const Handle(exeCSG_DrillHoleNode)& N,
                                               QWidget*                            parent)
: QDialog(parent), m_editedNode(N)
{
  this->construct();
}

//-----------------------------------------------------------------------------

//! Destructor.
exeCSG_DialogDrillHole::~exeCSG_DialogDrillHole()
{
  delete m_pMainLayout;
  m_widgets.Release();
}

//-----------------------------------------------------------------------------

//! Reaction on proceed.
void exeCSG_DialogDrillHole::onProceed()
{
  // Convert user input to double
  const double radius = QVariant( m_widgets.pRadius ->text() ).toDouble();
  const double depth  = QVariant( m_widgets.pDepth  ->text() ).toDouble();

  Handle(exeCSG_CommonFacilities) cf = exeCSG_CommonFacilities::Instance();

  // Modify Data Model
  Handle(exeCSG_DrillHoleNode) featureNode;
  //
  if ( m_editedNode.IsNull() )
  {
    cf->Model->OpenCommand();
    {
      featureNode = exeCSG_Commands(cf->Model).CreateDrillHoleNode(m_placement, radius, depth);

      // Execute functions
      cf->Model->FuncExecuteAll();
    }
    cf->Model->CommitCommand();
    //
    cf->ObjectBrowser->Populate();
  }
  else
  {
    featureNode = m_editedNode;

    cf->Model->OpenCommand();
    {
      m_editedNode->SetRadius(radius);
      m_editedNode->SetDepth(depth);

      // Execute functions
      cf->Model->FuncExecuteAll();
    }
    cf->Model->CommitCommand();
  }

  // Actualize
  cf->Prs.Stock->Actualize(featureNode, false, false);
  cf->Prs.Workpiece->Actualize(cf->Model->GetWorkpieceNode(), false, false);

  // Close
  this->close();
}

//-----------------------------------------------------------------------------

//! Constructs the controls.
void exeCSG_DialogDrillHole::construct()
{
  // Main layout
  m_pMainLayout = new QVBoxLayout();

  // Group box for parameters
  QGroupBox* pGroup = new QGroupBox("Parameters");

  // Editors
  m_widgets.pRadius = new asiUI_LineEdit();
  m_widgets.pDepth  = new asiUI_LineEdit();

  // Sizing
  m_widgets.pRadius ->setMinimumWidth(CONTROL_EDIT_WIDTH);
  m_widgets.pDepth  ->setMinimumWidth(CONTROL_EDIT_WIDTH);

  //---------------------------------------------------------------------------
  // Buttons
  //---------------------------------------------------------------------------

  m_widgets.pProceed = new QPushButton("Proceed");

  // Sizing
  m_widgets.pProceed->setMaximumWidth(CONTROL_BTN_WIDTH);

  // Reaction
  connect( m_widgets.pProceed, SIGNAL( clicked() ), this, SLOT( onProceed() ) );

  //---------------------------------------------------------------------------
  // Line editors
  //---------------------------------------------------------------------------

  // Create layout
  QGridLayout* pGrid = new QGridLayout(pGroup);
  //
  pGrid->addWidget(new QLabel("Radius:"), 0, 0);
  pGrid->addWidget(new QLabel("Depth:"),  1, 0);
  //
  pGrid->addWidget(m_widgets.pRadius, 0, 1);
  pGrid->addWidget(m_widgets.pDepth,  1, 1);
  //
  pGrid->setSpacing(5);
  pGrid->setColumnStretch(0, 0);
  pGrid->setColumnStretch(1, 1);
  pGrid->setAlignment(Qt::AlignTop | Qt::AlignLeft);

  //---------------------------------------------------------------------------
  // Main layout
  //---------------------------------------------------------------------------

  // Configure main layout
  m_pMainLayout->addWidget(pGroup);
  m_pMainLayout->addWidget(m_widgets.pProceed);
  m_pMainLayout->setAlignment(Qt::AlignTop | Qt::AlignLeft);
  m_pMainLayout->setContentsMargins(10, 10, 10, 10);

  this->setLayout(m_pMainLayout);
  this->setWindowModality(Qt::WindowModal);
  this->setWindowTitle("Drill hole");

  //---------------------------------------------------------------------------
  // Initialize
  //---------------------------------------------------------------------------

  // Set to UI controls
  if ( m_editedNode.IsNull() )
  {
    m_widgets.pRadius ->setText("1.0");
    m_widgets.pDepth  ->setText("1.0");
  }
  else
  {
    m_widgets.pRadius ->setText( QString::number( m_editedNode->GetRadius() ) );
    m_widgets.pDepth  ->setText( QString::number( m_editedNode->GetDepth() ) );
  }
}
