//-----------------------------------------------------------------------------
// Created on: 22 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

// Own include
#include <exeCSG_Controls.h>

// exeCSG includes
#include <exeCSG_CommonFacilities.h>
#include <exeCSG_DialogInitStock.h>
#include <exeCSG_Graph.h>

// asiUI includes
#include <asiUI_DialogOCAFDump.h>

// Qt include
#include <QGroupBox>

//-----------------------------------------------------------------------------

#define BTN_MIN_WIDTH 120

//-----------------------------------------------------------------------------

//! Constructor.
//! \param parent [in] parent widget.
exeCSG_Controls::exeCSG_Controls(QWidget* parent) : QWidget(parent)
{
  Handle(exeCSG_CommonFacilities) cf = exeCSG_CommonFacilities::Instance();

  // Main layout
  m_pMainLayout = new QVBoxLayout();

  // Buttons
  m_widgets.pInitStock = new QPushButton("Set stock parameters");
  m_widgets.pShowGraph = new QPushButton("Show construction graph");
  m_widgets.pDumpOCAF  = new QPushButton("Dump OCAF structure");
  //
  m_widgets.pInitStock -> setMinimumWidth(BTN_MIN_WIDTH);
  m_widgets.pShowGraph -> setMinimumWidth(BTN_MIN_WIDTH);
  m_widgets.pDumpOCAF  -> setMinimumWidth(BTN_MIN_WIDTH);

  // Group for modeling
  QGroupBox*   pModelingGroup = new QGroupBox("Modeling");
  QVBoxLayout* pModelingLay   = new QVBoxLayout(pModelingGroup);
  //
  pModelingLay->addWidget(m_widgets.pInitStock);

  // Group for tools
  QGroupBox*   pToolsGroup = new QGroupBox("Tools");
  QVBoxLayout* pToolsLay   = new QVBoxLayout(pToolsGroup);
  //
  pToolsLay->addWidget(m_widgets.pShowGraph);
  pToolsLay->addWidget(m_widgets.pDumpOCAF);

  // Set layout
  m_pMainLayout->addWidget(pModelingGroup);
  m_pMainLayout->addWidget(pToolsGroup);
  //
  m_pMainLayout->setAlignment(Qt::AlignTop);
  //
  this->setLayout(m_pMainLayout);

  // Connect signals to slots
  connect( m_widgets.pInitStock, SIGNAL( clicked() ), SLOT( onInitStock() ) );
  connect( m_widgets.pShowGraph, SIGNAL( clicked() ), SLOT( onShowGraph() ) );
  connect( m_widgets.pDumpOCAF,  SIGNAL( clicked() ), SLOT( onDumpOCAF() ) );
}

//-----------------------------------------------------------------------------

//! Destructor.
exeCSG_Controls::~exeCSG_Controls()
{
  delete m_pMainLayout;
  m_widgets.Release();
}

//-----------------------------------------------------------------------------

//! Allows to initialize stock parameters.
void exeCSG_Controls::onInitStock()
{
  exeCSG_DialogInitStock* pDlg = new exeCSG_DialogInitStock(this);
  //
  pDlg->show();
}

//-----------------------------------------------------------------------------

//! Shows construction graph.
void exeCSG_Controls::onShowGraph()
{
  Handle(exeCSG_CommonFacilities) cf = exeCSG_CommonFacilities::Instance();

  // Render graph
  exeCSG_Graph* pTreeView = new exeCSG_Graph(cf->Model);
  //
  pTreeView->Render();
}

//-----------------------------------------------------------------------------

//! Dumps OCAF to string.
void exeCSG_Controls::onDumpOCAF()
{
  Handle(exeCSG_CommonFacilities) cf = exeCSG_CommonFacilities::Instance();

  // Dump OCAF
  asiUI_DialogOCAFDump* pOCAFDump = new asiUI_DialogOCAFDump(cf->Model, cf->ProgressNotifier, this);
  //
  pOCAFDump->show();
}
