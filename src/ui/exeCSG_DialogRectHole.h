//-----------------------------------------------------------------------------
// Created on: 23 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_DialogRectHole_h
#define exeCSG_DialogRectHole_h

// asiUI includes
#include <asiUI_LineEdit.h>

// OCCT includes
#include <gp_Ax3.hxx>

// Qt includes
#pragma warning(push, 0)
#include <QDialog>
#include <QPushButton>
#include <QVBoxLayout>
#pragma warning(pop)

//! Controls for rectangular hole feature.
class exeCSG_DialogRectHole : public QDialog
{
  Q_OBJECT

public:

  exeCSG_DialogRectHole(const gp_Ax3& placement,
                        QWidget*      parent = NULL);

  virtual ~exeCSG_DialogRectHole();

public slots:

  void onProceed();

protected:

  //! Widgets.
  struct t_widgets
  {
  //---------------------------------------------------------------------------
    QPushButton*    pProceed; //!< Proceed with constructing hole.
  //---------------------------------------------------------------------------
    asiUI_LineEdit* pWidth;   //!< Width.
    asiUI_LineEdit* pHeight;  //!< Height.
    asiUI_LineEdit* pDepth;   //!< Depth.
  //---------------------------------------------------------------------------

    t_widgets() : pProceed (NULL),
                  pWidth   (NULL),
                  pHeight  (NULL),
                  pDepth   (NULL)
    {}

    void Release()
    {
      delete pProceed; pProceed = NULL;
      delete pWidth;   pWidth   = NULL;
      delete pHeight;  pHeight  = NULL;
      delete pDepth;   pDepth   = NULL;
    }
  };

  t_widgets    m_widgets;     //!< UI controls.
  QVBoxLayout* m_pMainLayout; //!< Layout of the widget.

protected:

  gp_Ax3 m_placement; //!< Placement axes (LCS for the feature).

};

#endif
