//-----------------------------------------------------------------------------
// Created on: 26 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_ViewerStockListener_h
#define exeCSG_ViewerStockListener_h

// asiUI includes
#include <asiUI_ViewerPartListener.h>

//! Custom interaction with 3D viewer.
class exeCSG_ViewerStockListener : public asiUI_ViewerPartListener
{
  Q_OBJECT

public:

  exeCSG_ViewerStockListener();

  virtual
    ~exeCSG_ViewerStockListener();

public:

  virtual void
    Connect();

protected slots:

  void onFacePicked(const asiVisu_PickResult&);

};

#endif
