//-----------------------------------------------------------------------------
// Created on: 22 June 2017
// Created by: Sergey SLYADNEV
//-----------------------------------------------------------------------------
// Web: http://dev.opencascade.org/
//-----------------------------------------------------------------------------

#ifndef exeCSG_DialogDrillHole_h
#define exeCSG_DialogDrillHole_h

// exeCSG includes
#include <exeCSG_DrillHoleNode.h>

// asiUI includes
#include <asiUI_LineEdit.h>

// OCCT includes
#include <gp_Ax3.hxx>

// Qt includes
#pragma warning(push, 0)
#include <QDialog>
#include <QPushButton>
#include <QVBoxLayout>
#pragma warning(pop)

//! Controls for drill hole feature.
class exeCSG_DialogDrillHole : public QDialog
{
  Q_OBJECT

public:

  exeCSG_DialogDrillHole(const gp_Ax3& placement,
                         QWidget*      parent = NULL);

  exeCSG_DialogDrillHole(const Handle(exeCSG_DrillHoleNode)& N,
                         QWidget*                            parent = NULL);

  virtual ~exeCSG_DialogDrillHole();

public slots:

  void onProceed();

protected:

  void construct();

protected:

  //! Widgets.
  struct t_widgets
  {
  //---------------------------------------------------------------------------
    QPushButton*    pProceed; //!< Proceed with constructing hole.
  //---------------------------------------------------------------------------
    asiUI_LineEdit* pRadius;  //!< Hole radius.
    asiUI_LineEdit* pDepth;   //!< Hole depth.
  //---------------------------------------------------------------------------

    t_widgets() : pProceed (NULL),
                  pRadius  (NULL),
                  pDepth   (NULL)
    {}

    void Release()
    {
      delete pProceed; pProceed = NULL;
      delete pRadius;  pRadius  = NULL;
      delete pDepth;   pDepth   = NULL;
    }
  };

  t_widgets    m_widgets;     //!< UI controls.
  QVBoxLayout* m_pMainLayout; //!< Layout of the widget.

protected:

  Handle(exeCSG_DrillHoleNode) m_editedNode; //!< Node to edit.
  gp_Ax3                       m_placement;  //!< Placement axes (LCS for the feature).

};

#endif
