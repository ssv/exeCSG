exeCSG is a prototype based on Analysis Situs software which implements the ideas presented in Manifold Geometry
blog (http://quaoar.su/blog/page/k-parametricheskomu-modelirovaniju-na-opencascade).

The prototype is based on Active Data, Qt, VTK, OpenCascade and Analysis Situs SDK (not to mention other dependencies
like Tcl/Tk, TBB, etc.).

All 3-rd parties can be downloaded in a single pack from here: http://analysissitus.org/files/exeCSG-products/exeCSG-products_win64-vc12_2017-12-16.zip

To see the prototype in action, you may check Youtube video here: https://www.youtube.com/watch?v=L5oB-bh99IQ

*See also:*

- Analysis Situs: https://bitbucket.org/Quaoar/analysissitus
- Active Data: https://bitbucket.org/Quaoar/activedata
